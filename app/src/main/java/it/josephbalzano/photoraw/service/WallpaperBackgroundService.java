package it.josephbalzano.photoraw.service;

import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.List;

import it.josephbalzano.photoraw.api.DownloadPhotoInfoListListener;
import it.josephbalzano.photoraw.api.DownloadPhotoListListener;
import it.josephbalzano.photoraw.api.DownloadSearchPhotosListener;
import it.josephbalzano.photoraw.api.UnsplashAPI;
import it.josephbalzano.photoraw.model.Photo;
import it.josephbalzano.photoraw.model.SearchResult;

/**
 * Created by Joseph Balzano
 * Project: photoraw
 */

public class WallpaperBackgroundService extends BroadcastReceiver {
    private static final String TAG = "PRWallpaperService";

    private static Context mContext = null;
    private static String mSource = null;

    @Override
    public void onReceive(final Context context, Intent intent) {
        startDownload(context, intent);
    }

    private static void setWallpaper(String url) {
        Log.d(TAG, "I'm setting your new wallpaper!");
        Bitmap bitmap = getBitmapFromURL(url);
        if (bitmap != null) {
            WallpaperManager wallpaperManager = WallpaperManager.getInstance(mContext);
            try {
                wallpaperManager.setBitmap(bitmap);
            } catch (IOException ex) {
                Log.d(TAG, "Error in setting your new wallpaper!");
                ex.printStackTrace();
            }
        }
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            java.net.URL url = new java.net.URL(src);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = null;
            try {
                myBitmap = BitmapFactory.decodeStream(input);
            } catch (Exception ex) {
                Log.e(TAG, "OOM Exception :(");
            }
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void startDownload(final Context context, Intent intent) {
        Log.d(TAG, "I'm starting for update your wallpaper!");
        mContext = context;
        String source = "Random";
        if (intent.hasExtra("src")) {
            mSource = source = intent.getStringExtra("src");
        } else if (mSource != null) {
            source = mSource;
        }

        if (source == null) {
            return;
        }

        if (source.equals("Random") || source.equals("from search...")) {
            UnsplashAPI.getRandomPhoto(new DownloadPhotoInfoListListener() {
                @Override
                public void onSuccess(Photo photoInfo) {
                    if (photoInfo != null
                            && photoInfo.getUrls() != null
                            && !TextUtils.isEmpty(photoInfo.getUrls().getFull())) {
                        setWallpaper(photoInfo.getUrls().getFull());
                    }
                }

                @Override
                public void onError() {
                    Log.d(TAG, "Error on get random photo :(");
                }
            });
        } else if (source.equals("Lastest")) {
            UnsplashAPI.getLastPopularPhoto(1, 1, new DownloadPhotoListListener() {
                @Override
                public void onSuccess(List<Photo> photoList) {
                    if (photoList != null && photoList.size() > 0) {
                        if (photoList.get(0) != null
                                && photoList.get(0).getUrls() != null
                                && !TextUtils.isEmpty(photoList.get(0).getUrls().getFull())) {
                            setWallpaper(photoList.get(0).getUrls().getFull());
                        }
                    }
                }

                @Override
                public void onError() {
                    Log.d(TAG, "Error on get lastest photo :(");
                }
            });
        } else {
            UnsplashAPI.getSearchPhotos(source, 1, 1, new DownloadSearchPhotosListener() {
                @Override
                public void onSuccess(SearchResult result) {
                    if (result != null) {
                        if (result.getResults() != null && result.getResults().size() > 0) {
                            if (result.getResults().get(0) != null
                                    && result.getResults().get(0).getUrls() != null
                                    && !TextUtils.isEmpty(result.getResults().get(0).getUrls().getFull())) {
                                setWallpaper(result.getResults().get(0).getUrls().getFull());
                            }
                        } else {
                            UnsplashAPI.getLastPopularPhoto(1, 1, new DownloadPhotoListListener() {
                                @Override
                                public void onSuccess(List<Photo> photoList) {
                                    if (photoList != null && photoList.size() > 0) {
                                        if (photoList.get(0) != null
                                                && photoList.get(0).getUrls() != null
                                                && !TextUtils.isEmpty(photoList.get(0).getUrls().getFull())) {
                                            setWallpaper(photoList.get(0).getUrls().getFull());
                                        }
                                    }
                                }

                                @Override
                                public void onError() {
                                    Log.d(TAG, "Error on get lastest photo :(");
                                }
                            });
                        }
                    }
                }

                @Override
                public void onError() {
                    Log.d(TAG, "Error on get searched photo :(");
                }
            });
        }
    }
}