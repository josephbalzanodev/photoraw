package it.josephbalzano.photoraw.manager;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by Joseph Balzano on 17/08/2017.
 */
public class HttpManager {
    private static final String TAG = "HttpManager";

    private static OkHttpClient mInstance;

    public static synchronized OkHttpClient getInstance() {
        if (mInstance == null) {
            mInstance = new OkHttpClient.Builder()
                    .connectTimeout(15, TimeUnit.SECONDS)
                    .addNetworkInterceptor(new StethoInterceptor())
                    .build();
        }
        return mInstance;
    }
}
