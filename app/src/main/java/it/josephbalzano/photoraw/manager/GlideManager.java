package it.josephbalzano.photoraw.manager;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by Joseph Balzano on 18/08/2017.
 */
@GlideModule
public final class GlideManager extends AppGlideModule {
}
