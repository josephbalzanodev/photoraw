package it.josephbalzano.photoraw.manager;

import android.app.Activity;
import android.os.AsyncTask;

import com.bumptech.glide.Glide;

/**
 * Created by Joseph Balzano
 * Project: photoraw
 */

public class CleanMemoryTask extends AsyncTask<Activity, Void, Void> {
    @Override
    protected Void doInBackground(Activity... activities) {
        for (Activity act : activities) {
            Glide.get(act).clearDiskCache();
        }
        return null;
    }
}
