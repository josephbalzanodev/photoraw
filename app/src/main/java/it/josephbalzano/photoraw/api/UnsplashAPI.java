package it.josephbalzano.photoraw.api;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import it.josephbalzano.photoraw.manager.HttpManager;
import it.josephbalzano.photoraw.model.Collection;
import it.josephbalzano.photoraw.model.Photo;
import it.josephbalzano.photoraw.model.SearchResult;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Joseph Balzano on 17/08/2017.
 */

public class UnsplashAPI {
    private static final String TAG = "UnsplashAPI";

    public static void getLastPopularPhoto(int page, int perPage,
                                           final DownloadPhotoListListener listener) {
        if (listener == null) {
            return;
        }

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.unsplash.com/photos").newBuilder();
        urlBuilder.addQueryParameter("page", String.format("%d", page));
        urlBuilder.addQueryParameter("per_page", String.format("%d", perPage));
        urlBuilder.addQueryParameter("order_by", "latest");
        urlBuilder.addQueryParameter("client_id", APIStrings.CLIENT_ID);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();

        HttpManager.getInstance()
                .newCall(request)
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        e.printStackTrace();
                        listener.onError();
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        if (!response.isSuccessful()) {
                            throw new IOException("Unexpected code " + response);
                        } else {
                            try {
                                Gson gson = new Gson();
                                Photo[] curatedPhoto = gson.fromJson(response.body().string(),
                                        Photo[].class);
                                if (curatedPhoto != null) {
                                    listener.onSuccess(Arrays.asList(curatedPhoto));
                                    return;
                                }
                                Log.e(TAG, "curatedPhoto is null");
                                listener.onError();
                            } catch (Exception ex) {
                                Log.e(TAG, "Error on parse curatedPhoto");
                                listener.onError();
                            }
                        }
                    }
                });
    }

    public static void getSearchPhotos(String query, int page, int perPage,
                                       final DownloadSearchPhotosListener listener) {
        if (listener == null) {
            return;
        }

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.unsplash.com/search/photos").newBuilder();
        urlBuilder.addQueryParameter("query", query);
        urlBuilder.addQueryParameter("page", String.format("%d", page));
        urlBuilder.addQueryParameter("per_page", String.format("%d", perPage));
        urlBuilder.addQueryParameter("client_id", APIStrings.CLIENT_ID);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();

        HttpManager.getInstance()
                .newCall(request)
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        e.printStackTrace();
                        listener.onError();
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        if (!response.isSuccessful()) {
                            throw new IOException("Unexpected code " + response);
                        } else {
                            try {
                                Gson gson = new Gson();
                                SearchResult searchedPhotos = gson.fromJson(response.body().string(),
                                        SearchResult.class);
                                if (searchedPhotos != null) {
                                    listener.onSuccess(searchedPhotos);
                                    return;
                                }
                                Log.e(TAG, "searchedPhotos is null");
                                listener.onError();
                            } catch (Exception ex) {
                                Log.e(TAG, "Error on parse searchedPhotos");
                                listener.onError();
                            }
                        }
                    }
                });
    }

    public static void getPhotoInfo(String photoId,
                                    final DownloadPhotoInfoListListener listener) {
        if (listener == null) {
            return;
        }

        HttpUrl.Builder urlBuilder = HttpUrl.parse(
                String.format("https://api.unsplash.com/photos/%s", photoId)).newBuilder();
        urlBuilder.addQueryParameter("client_id", APIStrings.CLIENT_ID);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();

        HttpManager.getInstance().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                listener.onError();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    try {
                        Gson gson = new Gson();
                        Photo photo = gson.fromJson(response.body().string(), Photo.class);
                        if (photo != null) {
                            if (!TextUtils.isEmpty(photo.getCreatedAt())) {
                                photo.setCreatedDate(getDateObjFromString(photo.getCreatedAt()));
                            }
                            if (!TextUtils.isEmpty(photo.getUpdatedAt())) {
                                photo.setUpdateDate(getDateObjFromString(photo.getUpdatedAt()));
                            }
                            listener.onSuccess(photo);
                            return;
                        }
                        Log.e(TAG, "Photo Info is null");
                        listener.onError();
                    } catch (Exception ex) {
                        Log.e(TAG, "Error on parse Photo Info");
                        listener.onError();
                    }
                }
            }
        });
    }

    public static void getRandomPhoto(final DownloadPhotoInfoListListener listener) {
        if (listener == null) {
            return;
        }

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.unsplash.com/photos/random").newBuilder();
        urlBuilder.addQueryParameter("client_id", APIStrings.CLIENT_ID);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();

        HttpManager.getInstance().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                listener.onError();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    try {
                        Gson gson = new Gson();
                        Photo photo = gson.fromJson(response.body().string(), Photo.class);
                        if (photo != null) {
                            if (!TextUtils.isEmpty(photo.getCreatedAt())) {
                                photo.setCreatedDate(getDateObjFromString(photo.getCreatedAt()));
                            }
                            if (!TextUtils.isEmpty(photo.getUpdatedAt())) {
                                photo.setUpdateDate(getDateObjFromString(photo.getUpdatedAt()));
                            }
                            listener.onSuccess(photo);
                            return;
                        }
                        listener.onError();
                        Log.e(TAG, "curatedPhoto is null");
                    } catch (Exception ex) {
                        listener.onError();
                        Log.e(TAG, "Error on parse curatedPhoto");
                    }
                }
            }
        });
    }

    public static void getFeaturedCollection(int page, int perPage,
                                             final DownloadCollectionListListener listener) {
        if (listener == null) {
            return;
        }

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.unsplash.com/collections/featured").newBuilder();
        urlBuilder.addQueryParameter("page", String.format("%d", page));
        urlBuilder.addQueryParameter("per_page", String.format("%d", perPage));
        urlBuilder.addQueryParameter("client_id", APIStrings.CLIENT_ID);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();

        HttpManager.getInstance()
                .newCall(request)
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        listener.onError();
                        e.printStackTrace();
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        if (!response.isSuccessful()) {
                            throw new IOException("Unexpected code " + response);
                        } else {
                            try {
                                Gson gson = new Gson();
                                Collection[] featuredCollections = gson.fromJson(response.body().string(),
                                        Collection[].class);
                                if (featuredCollections != null) {
                                    listener.onSuccess(Arrays.asList(featuredCollections));
                                    return;
                                }
                                listener.onError();
                                Log.e(TAG, "featuredCollections is null");
                            } catch (Exception ex) {
                                listener.onError();
                                Log.e(TAG, "Error on parse featuredCollections");
                            }
                        }
                    }
                });
    }

    public static void getCollectionPhotos(String collectionId, int page, int perPage,
                                           final DownloadPhotoListListener listener) {
        if (listener == null) {
            return;
        }

        HttpUrl.Builder urlBuilder = HttpUrl.parse(String.format("https://api.unsplash.com/collections/%s/photos", collectionId)).newBuilder();
        urlBuilder.addQueryParameter("page", String.format("%d", page));
        urlBuilder.addQueryParameter("per_page", String.format("%d", perPage));
        urlBuilder.addQueryParameter("client_id", APIStrings.CLIENT_ID);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();

        HttpManager.getInstance()
                .newCall(request)
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        e.printStackTrace();
                        listener.onError();
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        if (!response.isSuccessful()) {
                            throw new IOException("Unexpected code " + response);
                        } else {
                            try {
                                Gson gson = new Gson();
                                Photo[] curatedPhoto = gson.fromJson(response.body().string(),
                                        Photo[].class);
                                if (curatedPhoto != null) {
                                    listener.onSuccess(Arrays.asList(curatedPhoto));
                                    return;
                                }
                                Log.e(TAG, "curatedPhoto is null");
                                listener.onError();
                            } catch (Exception ex) {
                                Log.e(TAG, "Error on parse curatedPhoto");
                                listener.onError();
                            }
                        }
                    }
                });
    }

    private static Date getDateObjFromString(String dateStr) {
        Date dateReturn = null;
        try {
            if (!TextUtils.isEmpty(dateStr)) {
                //2017-07-07T22:58:39-04:00
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
                dateReturn = dateFormat.parse(dateStr);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateReturn;
    }
}
