package it.josephbalzano.photoraw.api;

import it.josephbalzano.photoraw.model.SearchResult;

/**
 * Created by Joseph Balzano on 29/08/2017.
 */

public interface DownloadSearchPhotosListener {
    public void onSuccess(SearchResult result);

    public void onError();
}