package it.josephbalzano.photoraw.api;

import java.util.List;

import it.josephbalzano.photoraw.model.Photo;

/**
 * Created by Joseph Balzano on 17/08/2017.
 */

public interface DownloadPhotoListListener {
    public void onSuccess(List<Photo> photoList);

    public void onError();
}
