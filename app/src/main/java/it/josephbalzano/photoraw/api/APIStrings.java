package it.josephbalzano.photoraw.api;

/**
 * Created by Joseph Balzano on 17/08/2017.
 */

public class APIStrings {
    public static final String CLIENT_ID = "cea68b36c3234ee4b2738998d0460165801b06a4c37f3930aa21a49bb6d16701";
    public static final String CLIENT_ID_PARAM = "client_id";
    public static final String PAGE_PARAM = "page";
    public static final String PER_PAGE_PARAM = "per_page";
}
