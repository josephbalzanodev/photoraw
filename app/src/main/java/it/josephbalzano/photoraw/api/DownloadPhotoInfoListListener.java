package it.josephbalzano.photoraw.api;

import it.josephbalzano.photoraw.model.Photo;

/**
 * Created by Joseph Balzano on 17/08/2017.
 */
public interface DownloadPhotoInfoListListener {
    public void onSuccess(Photo photoInfo);

    public void onError();
}
