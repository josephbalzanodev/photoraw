package it.josephbalzano.photoraw.api;

import java.util.List;

import it.josephbalzano.photoraw.model.Collection;

/**
 * Created by Joseph Balzano on 30/08/2017.
 */

public interface DownloadCollectionListListener {
    public void onSuccess(List<Collection> collections);

    public void onError();
}
