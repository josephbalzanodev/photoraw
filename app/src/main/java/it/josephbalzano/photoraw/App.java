package it.josephbalzano.photoraw;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.device.yearclass.YearClass;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;
import it.josephbalzano.photoraw.util.PreferenceUtils;
import okhttp3.OkHttpClient;

/**
 * Created by Joseph Balzano on 24/08/2017.
 */

public class App extends Application {
    private static final String TAG = "App";

    private static final String KEYS_NOT_INITIALIZED = "empty_key";
    public static final String PERMISSION_JUST_REQUIRED = "permission_first";
    public static int mPhotoPerPage = 6;
    private int nActivityRunning = 0;
    private boolean isAppRunning = false;
    public static Map<Activity, BroadcastReceiver> mBroadcastReciverRegistered = new HashMap<>();
    public static FirebaseAnalytics mFirebaseAnalytics = null;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        MobileAds.initialize(this, "ca-app-pub-8280023857708714~2820123472");
        PreferenceUtils.init(this);

        new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .build();

        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                        .build());

        int year = YearClass.get(getApplicationContext());
        if (year >= 2015) {
            mPhotoPerPage = 22;
        } else if (year == 2014) {
            mPhotoPerPage = 16;
        } else if (year == 2013) {
            mPhotoPerPage = 10;
        } else if (year > 2010) {
            mPhotoPerPage = 8;
        }

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        /**
         * Analytics
         */
        Bundle bundle = new Bundle();
        bundle.putInt("SDKVersion", Build.VERSION.SDK_INT);
        bundle.putInt("YearClassification", year);
        bundle.putString("PhoneModel", Build.MODEL);

        if (PreferenceUtils.getInstance().getBoolean(KEYS_NOT_INITIALIZED, true)) {
            PreferenceUtils.getInstance().setBoolean(KEYS_NOT_INITIALIZED, false);

            PreferenceUtils.getInstance().setString(getString(R.string.settings_download_key),
                    getString(R.string.settings_download_default_value));

            PreferenceUtils.getInstance().setString(getString(R.string.settings_timing_key),
                    getString(R.string.settings_timing_default_value));

            PreferenceUtils.getInstance().setBoolean(getString(R.string.settings_disable_fullscreen_key),
                    getResources().getBoolean(R.bool.settings_disable_fullscreen_default_value));

            PreferenceUtils.getInstance().setBoolean(getString(R.string.tutorial_key_collections), true);
            PreferenceUtils.getInstance().setBoolean(getString(R.string.tutorial_key_photo_info), true);
            PreferenceUtils.getInstance().setBoolean(getString(R.string.tutorial_key_wallpaper_from_home), true);

            bundle.putBoolean("FirstOpen", true);
        } else {
            bundle.putBoolean("FirstOpen", false);
        }

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle bundle) {

            }

            @Override
            public void onActivityStarted(Activity activity) {
                nActivityRunning++;
                if (!isAppRunning && nActivityRunning >= 1) {
                    isAppRunning = true;
                }
            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {
                nActivityRunning--;
                if (nActivityRunning == 0) {
                    isAppRunning = false;
                    for (Map.Entry<Activity, BroadcastReceiver> entry
                            : mBroadcastReciverRegistered.entrySet()) {
                        try {
                            (entry.getKey()).unregisterReceiver(entry.getValue());
                            Log.w(TAG, entry.getKey().toString() + " unregistered for prevent crash!");
                        } catch (Exception ex) {
                            Log.e(TAG, "Exception in unregisterReceiver: " + ex.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });
        App.mFirebaseAnalytics.logEvent("StartApp", bundle);
    }
}
