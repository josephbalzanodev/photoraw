package it.josephbalzano.photoraw.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Joseph Balzano on 30/08/2017.
 */

public class HintPreferences implements Serializable {
    @SerializedName("links")
    @Expose
    private List<String> hints;

    public List<String> getHints() {
        return hints;
    }

    public HintPreferences setHints(List<String> hints) {
        this.hints = hints;
        return this;
    }
}
