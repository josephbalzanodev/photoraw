package it.josephbalzano.photoraw.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Joseph Balzano on 30/08/2017.
 */

public class CollectionLinks extends Links implements Serializable {
    @SerializedName("photos")
    @Expose
    private String photos;
    @SerializedName("related")
    @Expose
    private String related;

    public CollectionLinks(String self, String html, String photos) {
        super(self, html);
        this.photos = photos;
    }

    public String getPhotos() {
        return photos;
    }

    public CollectionLinks setPhotos(String photos) {
        this.photos = photos;
        return this;
    }

    public String getRelated() {
        return related;
    }

    public CollectionLinks setRelated(String related) {
        this.related = related;
        return this;
    }
}
