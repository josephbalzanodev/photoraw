package it.josephbalzano.photoraw.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Joseph Balzano on 17/08/2017.
 */

public class PhotoLinks extends Links implements Serializable {
    @SerializedName("download")
    @Expose
    private String download;
    @SerializedName("download_location")
    @Expose
    private String downloadLocation;

    /**
     * @param downloadLocation
     * @param download
     * @param html
     * @param self
     */
    public PhotoLinks(String self, String html, String download, String downloadLocation) {
        super(self, html);
        this.download = download;
        this.downloadLocation = downloadLocation;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public String getDownloadLocation() {
        return downloadLocation;
    }

    public void setDownloadLocation(String downloadLocation) {
        this.downloadLocation = downloadLocation;
    }
}

