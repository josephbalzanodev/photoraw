package it.josephbalzano.photoraw.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Joseph Balzano on 17/08/2017.
 */

public class Photo implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("downloads")
    @Expose
    private Integer downloads;
    @SerializedName("likes")
    @Expose
    private Integer likes;
    @SerializedName("views")
    @Expose
    private Integer views;
    @SerializedName("liked_by_user")
    @Expose
    private Boolean likedByUser;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("exif")
    @Expose
    private Exif exif;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("urls")
    @Expose
    private Urls urls;
    @SerializedName("links")
    @Expose
    private PhotoLinks links;

    private transient Date createdDate;
    private transient Date updateDate;

    public Photo(String id, String createdAt, String updatedAt, Integer width, Integer height, String color, Integer likes, String slug, Integer downloads, Integer likes1, Integer views, Boolean likedByUser, String description, User user, Exif exif, Location location, Urls urls, PhotoLinks links) {
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.width = width;
        this.height = height;
        this.color = color;
        this.likes = likes;
        this.slug = slug;
        this.downloads = downloads;
        this.likes = likes1;
        this.views = views;
        this.likedByUser = likedByUser;
        this.description = description;
        this.user = user;
        this.exif = exif;
        this.location = location;
        this.urls = urls;
        this.links = links;
    }

    public Photo() {
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Photo setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public Photo setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public String getSlug() {
        return slug;
    }

    public Photo setSlug(String slug) {
        this.slug = slug;
        return this;
    }

    public Integer getDownloads() {
        return downloads;
    }

    public Photo setDownloads(Integer downloads) {
        this.downloads = downloads;
        return this;
    }

    public Integer getViews() {
        return views;
    }

    public Photo setViews(Integer views) {
        this.views = views;
        return this;
    }

    public Exif getExif() {
        return exif;
    }

    public Photo setExif(Exif exif) {
        this.exif = exif;
        return this;
    }

    public Location getLocation() {
        return location;
    }

    public Photo setLocation(Location location) {
        this.location = location;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Boolean getLikedByUser() {
        return likedByUser;
    }

    public void setLikedByUser(Boolean likedByUser) {
        this.likedByUser = likedByUser;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Urls getUrls() {
        return urls;
    }

    public void setUrls(Urls urls) {
        this.urls = urls;
    }

    public PhotoLinks getLinks() {
        return links;
    }

    public void setLinks(PhotoLinks links) {
        this.links = links;
    }

    public boolean isExtendedVersion() {
        return exif != null || location != null;
    }
}
