package it.josephbalzano.photoraw.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import it.josephbalzano.photoraw.R;

public class InfoItem extends LinearLayout {
    private String mTitle = "title";
    private String mDescription = "description";
    private Drawable mIcon = null;
    private View mView = null;
    private int mIconTint = Color.parseColor("#424242");

    public InfoItem(Context context) {
        super(context);
        init(null, 0);
    }

    public InfoItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public InfoItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        mView = inflate(getContext(), R.layout.info_item, null);

        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.InfoItem, defStyle, 0);

        mTitle = a.getString(R.styleable.InfoItem_title);
        mDescription = a.getString(R.styleable.InfoItem_description);
        mIcon = a.getDrawable(R.styleable.InfoItem_icon);
        mIconTint = a.getColor(R.styleable.InfoItem_iconTint, Color.parseColor("#424242"));

        if (mIcon != null) {
            ((ImageView) (mView.findViewById(R.id.icon))).setImageDrawable(mIcon);
            ((ImageView) (mView.findViewById(R.id.icon))).setColorFilter(mIconTint);
        } else {
            mView.findViewById(R.id.icon).setVisibility(GONE);
        }

        ((TextView) (mView.findViewById(R.id.first_label))).setText(mTitle);
        ((TextView) (mView.findViewById(R.id.second_label))).setText(mDescription);

        addView(mView);
    }

    public void setTitle(String title) {
        mTitle = title;
        ((TextView) (mView.findViewById(R.id.first_label))).setText(mTitle);
    }

    public void setDescription(String desc) {
        mDescription = desc;
        ((TextView) (mView.findViewById(R.id.second_label))).setText(mDescription);
    }

    public void setIcon(Drawable icon) {
        mIcon = icon;
        ((ImageView) (mView.findViewById(R.id.icon))).setImageDrawable(mIcon);
    }

    public void setIconTint(int iconTint) {
        mIconTint = iconTint;
        ((ImageView) (mView.findViewById(R.id.icon))).setColorFilter(mIconTint);
    }
}
