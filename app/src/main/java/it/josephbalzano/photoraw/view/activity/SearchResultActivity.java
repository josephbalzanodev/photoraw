package it.josephbalzano.photoraw.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;

import java.util.List;

import it.josephbalzano.photoraw.App;
import it.josephbalzano.photoraw.R;
import it.josephbalzano.photoraw.adapter.CuratedPhotoListAdapter;
import it.josephbalzano.photoraw.api.DownloadPhotoListListener;
import it.josephbalzano.photoraw.api.DownloadSearchPhotosListener;
import it.josephbalzano.photoraw.api.UnsplashAPI;
import it.josephbalzano.photoraw.model.Collection;
import it.josephbalzano.photoraw.model.Photo;
import it.josephbalzano.photoraw.model.SearchResult;

public class SearchResultActivity extends AppCompatActivity {
    public final static String QUERY_KEY = "query_key";
    public static final String COLLECTION = "collection_id";
    private Toolbar mToolbar = null;

    private String mQuery = null;
    private Collection mCollection = null;
    private RecyclerView mRecyclerResult = null;
    private LinearLayoutManager mRecyclerResultLayoutManager = null;
    private boolean mLoadingNext = false;
    private int mPhotoPage = 1;
    private int mLoadedPhoto = 0;
    private boolean mLockNextPagination = false;
    private CuratedPhotoListAdapter mCuratedAdapter;

    DownloadSearchPhotosListener mSearchPhotoListener = new DownloadSearchPhotosListener() {
        @Override
        public void onSuccess(final SearchResult result) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mCuratedAdapter = new CuratedPhotoListAdapter(result.getResults(), false, SearchResultActivity.this, true);
                    mRecyclerResult.setAdapter(mCuratedAdapter);
                    updateResultInfo(result.getTotal().toString());

                    mLoadedPhoto = result.getResults().size();
                    if (mLoadedPhoto >= result.getTotal()) {
                        mCuratedAdapter.removeLoading();
                        mLockNextPagination = true;
                        if (result.getTotal() == 0) {
                            it.josephbalzano.photoraw.util.ViewUtils
                                    .toFade(findViewById(R.id.empty_view), View.VISIBLE);
                        }
                    }
                    it.josephbalzano.photoraw.util.ViewUtils
                            .toFade(findViewById(R.id.progress_view), View.GONE);
                    mLoadingNext = false;
                    mPhotoPage++;

                    /**
                     * Analytics
                     */
                    Bundle bundle = new Bundle();
                    bundle.putString("Query", mQuery);
                    bundle.putInt("Result", result.getResults().size());
                    bundle.putInt("TotalResult", result.getTotal());
                    bundle.putInt("PageRequested", mPhotoPage);
                    App.mFirebaseAnalytics.logEvent("Search", bundle);
                }
            });
        }

        @Override
        public void onError() {
            it.josephbalzano.photoraw.util.ViewUtils
                    .toFade(findViewById(R.id.empty_view), View.VISIBLE);
        }
    };

    DownloadSearchPhotosListener mSearchPaginationPhotoListener = new DownloadSearchPhotosListener() {
        @Override
        public void onSuccess(final SearchResult result) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (result.getResults().size() > 0) {
                        mCuratedAdapter.addPhoto(result.getResults());
                        mCuratedAdapter.notifyItemInserted(mCuratedAdapter.getItemCount() - result.getResults().size());
                        updateResultInfo(result.getTotal().toString());

                        mLoadedPhoto += result.getResults().size();
                        if (mLoadedPhoto >= result.getTotal()) {
                            mCuratedAdapter.removeLoading();
                            mLockNextPagination = true;
                        }
                        mLoadingNext = false;
                        mPhotoPage++;
                    }

                    /**
                     * Analytics
                     */
                    Bundle bundle = new Bundle();
                    bundle.putString("Query", mQuery);
                    bundle.putInt("Result", result.getResults().size());
                    bundle.putInt("TotalResult", result.getTotal());
                    bundle.putInt("PageRequested", mPhotoPage);
                    App.mFirebaseAnalytics.logEvent("Search", bundle);
                }
            });
        }

        @Override
        public void onError() {

        }
    };

    DownloadPhotoListListener mCollectionPhotoListener = new DownloadPhotoListListener() {
        @Override
        public void onSuccess(final List<Photo> photoList) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mCuratedAdapter = new CuratedPhotoListAdapter(photoList, false,
                            SearchResultActivity.this, true);
                    mRecyclerResult.setAdapter(mCuratedAdapter);

                    it.josephbalzano.photoraw.util.ViewUtils.toFade(findViewById(R.id.progress_view), View.GONE);
                    mLoadingNext = false;
                    mPhotoPage++;

                    /**
                     * Analytics
                     */
                    Bundle bundle = new Bundle();
                    bundle.putString("CollectionTitle", mCollection.getTitle());
                    bundle.putInt("Result", photoList.size());
                    bundle.putInt("TotalResult", mCollection.getTotalPhotos());
                    bundle.putInt("PageRequested", mPhotoPage);
                    App.mFirebaseAnalytics.logEvent("CollectionView", bundle);
                }
            });
        }

        @Override
        public void onError() {
        }
    };

    DownloadPhotoListListener mCollectionPaginationPhotoListener = new DownloadPhotoListListener() {
        @Override
        public void onSuccess(final List<Photo> photoList) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (photoList.size() > 0) {
                        mCuratedAdapter.addPhoto(photoList);
                        mCuratedAdapter.notifyItemInserted(mCuratedAdapter.getItemCount() - photoList.size());

                        mLoadingNext = false;
                        mPhotoPage++;

                        /**
                         * Analytics
                         */
                        Bundle bundle = new Bundle();
                        bundle.putString("CollectionTitle", mCollection.getTitle());
                        bundle.putInt("Result", photoList.size());
                        bundle.putInt("TotalResult", mCollection.getTotalPhotos());
                        bundle.putInt("PageRequested", mPhotoPage);
                        App.mFirebaseAnalytics.logEvent("CollectionView", bundle);
                    }
                }
            });
        }

        @Override
        public void onError() {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRecyclerResult = (RecyclerView) findViewById(R.id.result_list);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent() != null && getIntent().hasExtra(QUERY_KEY)) {
            mQuery = getIntent().getStringExtra(QUERY_KEY);
            mCollection = null;
        } else if (getIntent() != null && getIntent().hasExtra(COLLECTION)) {
            mCollection = (Collection) getIntent().getSerializableExtra(COLLECTION);
            mQuery = null;
        } else {
            finish();
            return;
        }

        if (!TextUtils.isEmpty(mQuery)) {
            getSupportActionBar().setTitle("Result for: " + mQuery);
        } else if (mCollection != null) {
            getSupportActionBar().setTitle(mCollection.getTitle());
        }

        mRecyclerResultLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerResult.setLayoutManager(mRecyclerResultLayoutManager);
        mRecyclerResult.setItemAnimator(new DefaultItemAnimator());

        mRecyclerResult.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    if (mLockNextPagination) {
                        return;
                    }
                    int visibleItemCount = mRecyclerResultLayoutManager.getChildCount();
                    int totalItemCount = mRecyclerResultLayoutManager.getItemCount();
                    int pastVisiblesItems = mRecyclerResultLayoutManager.findFirstVisibleItemPosition();
                    if (!mLoadingNext) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            mLoadingNext = true;

                            if (!TextUtils.isEmpty(mQuery)) {
                                UnsplashAPI.getSearchPhotos(mQuery, mPhotoPage, App.mPhotoPerPage,
                                        mSearchPaginationPhotoListener);
                            } else if (mCollection != null) {
                                UnsplashAPI.getCollectionPhotos(mCollection.getId().toString(), mPhotoPage, App.mPhotoPerPage,
                                        mCollectionPaginationPhotoListener);
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        if (!TextUtils.isEmpty(mQuery)) {
            UnsplashAPI.getSearchPhotos(mQuery, mPhotoPage, App.mPhotoPerPage,
                    mSearchPhotoListener);
        } else if (mCollection != null) {
            UnsplashAPI.getCollectionPhotos(mCollection.getId().toString(), mPhotoPage, App.mPhotoPerPage,
                    mCollectionPhotoListener);
        }
    }

    private void updateResultInfo(String result) {
        getSupportActionBar().setTitle("Result for: " + mQuery + " ( " + result + " )");
    }
}
