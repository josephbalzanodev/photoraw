package it.josephbalzano.photoraw.view.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import it.josephbalzano.photoraw.App;
import it.josephbalzano.photoraw.R;
import it.josephbalzano.photoraw.manager.CleanMemoryTask;
import it.josephbalzano.photoraw.util.PreferenceUtils;
import it.josephbalzano.photoraw.util.StringUtils;

import static it.josephbalzano.photoraw.adapter.SearchHintAdapter.HINT_PREFERENCE_KEY;

/**
 * Created by Joseph Balzano on 23/08/2017.
 */
public class SettingsActivity extends AppCompatActivity {
    private Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("Settings");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getFragmentManager().beginTransaction().replace(R.id.settings_container,
                new SettingsFragment()).commit();
    }

    public static class SettingsFragment extends PreferenceFragment implements RewardedVideoAdListener, SharedPreferences.OnSharedPreferenceChangeListener {
        private RewardedVideoAd mAd;
        private Bundle mBundle = new Bundle();

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);

            mAd = MobileAds.getRewardedVideoAdInstance(getActivity());
            mAd.setRewardedVideoAdListener(this);

            Preference buttonDeleteSearchHint = findPreference(getString(R.string.settings_delete_search_key));
            Preference buttonAdv = findPreference("startAdv");
            Preference buttonDeleteCache = findPreference(getString(R.string.settings_delete_cache_key));
            if (PreferenceUtils.getInstance().getHintPreference(HINT_PREFERENCE_KEY, null) == null) {
                buttonDeleteSearchHint.setEnabled(false);
            }

            buttonAdv.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Toast.makeText(getActivity(), "Video it's starting! Thanks to much", Toast.LENGTH_SHORT).show();
                    loadRewardedVideoAd();
                    return true;
                }
            });

            buttonDeleteSearchHint.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    PreferenceUtils.getInstance().removePreference(HINT_PREFERENCE_KEY);
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Now you're safe! All test have been deleted "
                                    + StringUtils.getEmojiByUnicode(0x1F609), Toast.LENGTH_LONG).show();
                    preference.setEnabled(false);
                    return true;
                }
            });

            buttonDeleteCache.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    new CleanMemoryTask().execute(getActivity());
                    Glide.get(getActivity()).clearMemory();
                    Toast.makeText(getActivity().getApplicationContext(),
                            "I should have cleaned all the dirt left by the printed films "
                                    + StringUtils.getEmojiByUnicode(0x1F39E), Toast.LENGTH_LONG).show();
                    preference.setEnabled(false);
                    return true;
                }
            });
        }

        private void loadRewardedVideoAd() {
            mAd.loadAd("ca-app-pub-8280023857708714/8164961251", new AdRequest.Builder().build());
        }

        @Override
        public void onResume() {
            super.onResume();
            mAd.resume(getActivity());

            getPreferenceScreen().getSharedPreferences()
                    .registerOnSharedPreferenceChangeListener(this);
            //disableTiming();
        }

        @Override
        public void onPause() {
            super.onPause();
            mAd.pause(getActivity());
            getPreferenceScreen().getSharedPreferences()
                    .unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onDestroy() {
            mAd.destroy(getActivity());
            super.onDestroy();
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            //disableTiming();
        }

        private void disableTiming() {
            if (PreferenceUtils.getInstance().getBoolean(getString(R.string.settings_disable_fullscreen_key), true)) {
                Preference pref = getPreferenceManager().findPreference(getString(R.string.settings_timing_key));
                pref.setEnabled(false);
            } else {
                Preference pref = getPreferenceManager().findPreference(getString(R.string.settings_timing_key));
                pref.setEnabled(true);
            }
        }

        @Override
        public void onRewardedVideoAdLoaded() {
            if (mAd.isLoaded()) {
                mAd.show();
                mBundle.clear();
            }
        }

        @Override
        public void onRewardedVideoAdOpened() {
        }

        @Override
        public void onRewardedVideoStarted() {
        }

        @Override
        public void onRewardedVideoAdClosed() {
            mBundle.putBoolean("CompletedView", false);
            App.mFirebaseAnalytics.logEvent("WatchedAd", mBundle);
        }

        @Override
        public void onRewarded(RewardItem rewardItem) {
            mBundle.putBoolean("CompletedView", true);
            App.mFirebaseAnalytics.logEvent("WatchedAd", mBundle);
        }

        @Override
        public void onRewardedVideoAdLeftApplication() {

        }

        @Override
        public void onRewardedVideoAdFailedToLoad(int i) {

        }
    }
}
