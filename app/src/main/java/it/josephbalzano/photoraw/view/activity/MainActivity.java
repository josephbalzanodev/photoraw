package it.josephbalzano.photoraw.view.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;

import java.util.List;

import it.josephbalzano.photoraw.App;
import it.josephbalzano.photoraw.R;
import it.josephbalzano.photoraw.adapter.CuratedPhotoListAdapter;
import it.josephbalzano.photoraw.api.DownloadPhotoListListener;
import it.josephbalzano.photoraw.api.UnsplashAPI;
import it.josephbalzano.photoraw.model.Photo;
import it.josephbalzano.photoraw.util.PreferenceUtils;
import it.josephbalzano.photoraw.util.ViewUtils;
import it.josephbalzano.photoraw.view.custom.BackgroundDialogFragment;
import it.josephbalzano.photoraw.view.custom.InfoDialogFragment;

import static it.josephbalzano.photoraw.App.PERMISSION_JUST_REQUIRED;
import static it.josephbalzano.photoraw.view.activity.PhotoFullscreenActivity.RANDOM_CONTENT;

/**
 * Import for Auth
 * import com.google.android.gms.common.api.GoogleApiClient;
 * import com.google.android.gms.tasks.OnCompleteListener;
 * import com.google.android.gms.tasks.Task;
 * import com.google.firebase.auth.AuthCredential;
 * import com.google.firebase.auth.AuthResult;
 * import com.google.firebase.auth.FirebaseAuth;
 * import com.google.firebase.auth.FirebaseUser;
 * import com.google.firebase.auth.GoogleAuthProvider;
 * import com.google.android.gms.auth.api.Auth;
 * import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
 * import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
 * import com.google.android.gms.auth.api.signin.GoogleSignInResult;
 */

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener /*, GoogleApiClient.OnConnectionFailedListener*/ {
    private static final String TAG = "MainActivity";

    private static final int RC_SIGN_IN = 0x8;
    private DrawerLayout mDrawerLayout = null;
    private RecyclerView mCuratedPhotoList = null;
    private LinearLayoutManager mCuratedPhotoListLayoutManager = null;
    private CuratedPhotoListAdapter mCuratedAdapter = null;
    private int mPhotoPage = 1;
    private boolean mLoadingNext = false;
    private boolean mPostPermissionGranted = false;

    private TextView mTextWallpaperService = null;

    /* Auth
    private GoogleApiClient mGoogleApiClient;
    private FirebaseAuth mAuth = null;
    */

    DownloadPhotoListListener mCuratedPhotoListener = new DownloadPhotoListListener() {
        @Override
        public void onSuccess(final List<Photo> photoList) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mCuratedAdapter = new CuratedPhotoListAdapter(photoList, false, MainActivity.this, true);
                    mCuratedPhotoList.setAdapter(mCuratedAdapter);

                    ViewUtils.toFade(findViewById(R.id.progress_view), View.GONE);
                    ViewUtils.toFade(findViewById(R.id.bad_view), View.GONE);
                    mLoadingNext = false;
                    mPhotoPage++;

                    if (PreferenceUtils.getInstance().getBoolean(getString(R.string.tutorial_key_collections), false)) {
                        PreferenceUtils.getInstance().removePreference(getString(R.string.tutorial_key_collections));
                        TapTargetView.showFor(MainActivity.this,
                                TapTarget.forToolbarMenuItem((Toolbar) findViewById(R.id.toolbar),
                                        R.id.action_settings, "Collections and Search",
                                        "In this section you can find the best photo collections and search for more"));
                    }
                }
            });
        }

        @Override
        public void onError() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ViewUtils.toFade(findViewById(R.id.bad_view), View.VISIBLE);
                }
            });
        }
    };

    DownloadPhotoListListener mCuratedPaginationPhotoListener = new DownloadPhotoListListener() {
        @Override
        public void onSuccess(final List<Photo> photoList) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (photoList.size() > 0) {
                        mCuratedAdapter.addPhoto(photoList);
                        mCuratedAdapter.notifyItemInserted(mCuratedAdapter.getItemCount() - photoList.size());

                        mLoadingNext = false;
                        mPhotoPage++;
                    }
                }
            });
        }

        @Override
        public void onError() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mLoadingNext = false;
                    mCuratedAdapter.removeLoading();
                    mCuratedAdapter.notifyItemRemoved(mCuratedAdapter.getItemCount());
                }
            });
        }
    };
    private Menu mMenu = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* Setup Auth

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.default_web_client_id))
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mAuth = FirebaseAuth.getInstance();*/

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mCuratedPhotoList = (RecyclerView) findViewById(R.id.curated_list);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        mTextWallpaperService = (TextView) MenuItemCompat.getActionView(navigationView.getMenu()
                .findItem(R.id.nav_wallpaper_services));

        mTextWallpaperService.setGravity(Gravity.CENTER_VERTICAL);
        mTextWallpaperService.setTypeface(null, Typeface.BOLD);
        mTextWallpaperService.setTextColor(getResources().getColor(R.color.colorAccent));
        mTextWallpaperService.setText("BETA");

        mCuratedPhotoListLayoutManager = new LinearLayoutManager(getApplicationContext());
        mCuratedPhotoList.setLayoutManager(mCuratedPhotoListLayoutManager);
        mCuratedPhotoList.setItemAnimator(new DefaultItemAnimator());

        mCuratedPhotoList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItemCount = mCuratedPhotoListLayoutManager.getChildCount();
                    int totalItemCount = mCuratedPhotoListLayoutManager.getItemCount();
                    int pastVisiblesItems = mCuratedPhotoListLayoutManager.findFirstVisibleItemPosition();
                    if (!mLoadingNext) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            mLoadingNext = true;
                            UnsplashAPI.getLastPopularPhoto(mPhotoPage, App.mPhotoPerPage,
                                    mCuratedPaginationPhotoListener);
                        }
                    }
                }
            }
        });

        findViewById(R.id.retry_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.toFade(findViewById(R.id.progress_view), View.VISIBLE);
                ViewUtils.toFade(findViewById(R.id.bad_view), View.GONE);
                firstCall();
            }
        });
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        firstCall();
    }

    private void firstCall() {
        UnsplashAPI.getLastPopularPhoto(mPhotoPage, App.mPhotoPerPage, mCuratedPhotoListener);
        mLoadingNext = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        mMenu = menu;

        return true;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent goSearch = new Intent(getApplicationContext(), CollectionActivity.class);
            startActivity(goSearch);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_random_photo) {
            Intent goFullscreen = new Intent(getApplicationContext(), PhotoFullscreenActivity.class);
            goFullscreen.putExtra(RANDOM_CONTENT, "yes");
            startActivity(goFullscreen);
        } else if (id == R.id.nav_info) {
            InfoDialogFragment dialog = new InfoDialogFragment();
            dialog.show(getFragmentManager(), "info");
        } else if (id == R.id.nav_settings) {
            Intent goSettings = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(goSettings);
        }
        /* Gestione click menu Auth
        else if (id == R.id.nav_sign_in) {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }*/
        else if (id == R.id.nav_wallpaper_services) {
            BackgroundDialogFragment dialog = new BackgroundDialogFragment();
            dialog.setParentActivity(this);
            dialog.show(getFragmentManager(), "background");
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mPostPermissionGranted = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPostPermissionGranted) {
            mPostPermissionGranted = false;
            PreferenceUtils.getInstance().setBoolean(PERMISSION_JUST_REQUIRED, true);
            Toast.makeText(this, "You can now download photos and set it as your amazing wallpaper!"
                    , Toast.LENGTH_SHORT).show();
        }
    }

    /*  Auth functions

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.d(TAG, "LOGGED:" + acct.getDisplayName());
            GoogleSignInAccount account = result.getSignInAccount();
            firebaseAuthWithGoogle(account);
        } else {
            Log.d(TAG, "NOT LOGGED");
        }
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            DatabaseReference database = FirebaseDatabase.getInstance().getReference();
                            database.child(acct.getId()).child("savedPhotos").setValue("try1" + new Random().nextInt());
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }*/
}
