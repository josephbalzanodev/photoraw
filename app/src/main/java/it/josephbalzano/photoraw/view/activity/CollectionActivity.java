package it.josephbalzano.photoraw.view.activity;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import it.josephbalzano.photoraw.App;
import it.josephbalzano.photoraw.R;
import it.josephbalzano.photoraw.adapter.FeaturedCollectionsAdapter;
import it.josephbalzano.photoraw.adapter.FeaturedCollectionsDecoration;
import it.josephbalzano.photoraw.api.DownloadCollectionListListener;
import it.josephbalzano.photoraw.api.UnsplashAPI;
import it.josephbalzano.photoraw.model.Collection;
import it.josephbalzano.photoraw.util.PreferenceUtils;
import it.josephbalzano.photoraw.util.ViewUtils;
import it.josephbalzano.photoraw.view.fragment.SearchHintFragment;
import xyz.sahildave.widget.SearchViewLayout;

import static it.josephbalzano.photoraw.adapter.SearchHintAdapter.HINT_PREFERENCE_KEY;
import static it.josephbalzano.photoraw.view.activity.SearchResultActivity.QUERY_KEY;

public class CollectionActivity extends AppCompatActivity implements SearchHintFragment.OnSearchFromHint {
    private SearchHintFragment mHintFragment = null;
    private SearchViewLayout mSearchViewLayout = null;
    private Toolbar mToolbar = null;
    private RecyclerView mCollectionRecycler = null;
    private GridLayoutManager mCollectionLayoutManager = null;
    private FeaturedCollectionsAdapter mCollectionsAdapter = null;

    private DownloadCollectionListListener mCollectionsDownload = new DownloadCollectionListListener() {
        @Override
        public void onSuccess(final List<Collection> collections) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mCollectionsAdapter = new FeaturedCollectionsAdapter(collections, CollectionActivity.this);
                    mCollectionRecycler.setAdapter(mCollectionsAdapter);
                    int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.item_decoration_collections_space);
                    mCollectionRecycler.addItemDecoration(new FeaturedCollectionsDecoration(spacingInPixels, mCollectionsAdapter.getItemCount()));

                    it.josephbalzano.photoraw.util.ViewUtils
                            .toFade(findViewById(R.id.progress_view), View.GONE);
                }
            });
        }

        @Override
        public void onError() {
            it.josephbalzano.photoraw.util.ViewUtils
                    .toFade(findViewById(R.id.bad_view), View.VISIBLE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collections);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mSearchViewLayout = (SearchViewLayout) findViewById(R.id.search_view_container);
        mCollectionRecycler = (RecyclerView) findViewById(R.id.collection_list);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mCollectionLayoutManager = new GridLayoutManager(this, 2);
        mCollectionRecycler.setLayoutManager(mCollectionLayoutManager);
        mCollectionRecycler.setItemAnimator(new DefaultItemAnimator());

        mHintFragment = new SearchHintFragment();

        mSearchViewLayout.handleToolbarAnimation(mToolbar);
        mSearchViewLayout.setExpandedContentSupportFragment(this, mHintFragment);
        mSearchViewLayout.setCollapsedHint("Search beautiful photos");

        ColorDrawable collapsedColor = new ColorDrawable(ContextCompat
                .getColor(this, R.color.colorPrimary));
        ColorDrawable expandedColor = new ColorDrawable(ContextCompat
                .getColor(this, R.color.default_color_expanded));
        mSearchViewLayout.setTransitionDrawables(collapsedColor, expandedColor);
        mSearchViewLayout.setSearchListener(new SearchViewLayout.SearchListener() {
            @Override
            public void onFinished(String searchKeyword) {
                goToResult(searchKeyword, true);
            }
        });
        UnsplashAPI.getFeaturedCollection(1, App.mPhotoPerPage, mCollectionsDownload);

        findViewById(R.id.retry_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewUtils.toFade(findViewById(R.id.progress_view), View.VISIBLE);
                ViewUtils.toFade(findViewById(R.id.bad_view), View.GONE);
                UnsplashAPI.getFeaturedCollection(1, App.mPhotoPerPage, mCollectionsDownload);
            }
        });
    }

    @Override
    public void onClickOnHint(String label) {
        goToResult(label, false);
    }

    /**
     * Go to SearchResultActivity if query is valid and before had saved query
     *
     * @param query     Query to search
     * @param saveQuery
     */
    private void goToResult(String query, boolean saveQuery) {
        mSearchViewLayout.collapse();
        if (TextUtils.isEmpty(query) || !isValidQuery(query)) {
            Toast.makeText(this, "I'm sorry but I can't search: " + query, Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        if (saveQuery) {
            PreferenceUtils.getInstance().addToHintPreference(HINT_PREFERENCE_KEY, query);
        }

        Intent goResult = new Intent(getApplicationContext(), SearchResultActivity.class);
        goResult.putExtra(QUERY_KEY, query);
        startActivity(goResult);
    }

    private boolean isValidQuery(String query) {
        String regexPattern = "^[a-zA-Z0-9àèéìùò%'.\\s]{1,50}";
        return query.matches(regexPattern);
    }
}
