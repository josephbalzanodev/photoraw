package it.josephbalzano.photoraw.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import it.josephbalzano.photoraw.App;
import it.josephbalzano.photoraw.R;
import it.josephbalzano.photoraw.api.DownloadPhotoInfoListListener;
import it.josephbalzano.photoraw.api.UnsplashAPI;
import it.josephbalzano.photoraw.manager.GlideApp;
import it.josephbalzano.photoraw.model.Photo;
import it.josephbalzano.photoraw.util.PreferenceUtils;
import it.josephbalzano.photoraw.util.ViewUtils;
import it.josephbalzano.photoraw.view.custom.PermissionDialogFragment;
import it.josephbalzano.photoraw.view.custom.PhotoDownloadModal;
import it.josephbalzano.photoraw.view.custom.PhotoInfoModal;
import it.josephbalzano.photoraw.view.custom.PhotoShareModal;
import it.josephbalzano.photoraw.view.custom.PinchImageView;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;
import static android.os.Build.VERSION_CODES.LOLLIPOP;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static it.josephbalzano.photoraw.App.PERMISSION_JUST_REQUIRED;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class PhotoFullscreenActivity extends AppCompatActivity {
    private static final String TAG = "PhotoFullscreenActivity";

    public static final String PHOTO_CONTENT = "photo";
    public static final String RANDOM_CONTENT = "random_photo";

    private static final int UI_ANIMATION_DELAY = 300;

    private boolean isHideDisabled = true;
    private String mHideTiming = "1200ms";

    private final Handler mHideHandler = new Handler();
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            mPhotoView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            ViewUtils.toFade(mInfoToolbar, View.VISIBLE);
            ViewUtils.toFade(mURLButton, View.VISIBLE);
            if (mImageLoaded) {
                ViewUtils.toFade(mPhotoControls, View.VISIBLE);
            }
            if (mShuffleButtonShow) {
                ViewUtils.toFade(mShuffleButton, View.VISIBLE);
            }
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hideNavigationBar();
        }
    };

    private View.OnClickListener mInfoClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            PhotoInfoModal info = new PhotoInfoModal();
            info.setPhoto(mContent);
            info.setParentActivity(PhotoFullscreenActivity.this);
            info.show(getSupportFragmentManager(), "");
        }
    };

    private View.OnClickListener mShareClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            PhotoShareModal info = new PhotoShareModal();
            info.setPhoto(mContent);
            info.setParentActivity(PhotoFullscreenActivity.this);
            info.show(getSupportFragmentManager(), "");
        }
    };

    private View.OnClickListener mDownloadClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    //  Permission granted in history
                    PhotoDownloadModal downloadModal = new PhotoDownloadModal();
                    downloadModal.setActivity(PhotoFullscreenActivity.this);
                    downloadModal.setPhoto(mContent);
                    downloadModal.show(getSupportFragmentManager(), "");
                } else {
                    if (PreferenceUtils.getInstance().getBoolean(PERMISSION_JUST_REQUIRED, false)) {
                        PermissionDialogFragment dialogFragment = new PermissionDialogFragment();
                        dialogFragment.setParentActivity(PhotoFullscreenActivity.this);
                        dialogFragment.show(PhotoFullscreenActivity.this.getFragmentManager(),
                                "downloadDialog");
                    } else {
                        ActivityCompat.requestPermissions(PhotoFullscreenActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    }
                }
            } else {
                // SDK under 23
                PhotoDownloadModal downloadModal = new PhotoDownloadModal();
                downloadModal.setActivity(PhotoFullscreenActivity.this);
                downloadModal.setPhoto(mContent);
                downloadModal.show(getSupportFragmentManager(), "");
            }
        }
    };

    private Photo mContent = null;
    private PinchImageView mPhotoView = null;
    private ImageView mNoLoaded = null;
    private RelativeLayout mPhotoControls = null;
    private AppBarLayout mInfoToolbar = null;

    private TextView mDescription = null;
    private TextView mUser = null;
    private FloatingActionButton mShuffleButton = null;
    private FloatingActionButton mURLButton = null;
    private boolean mShuffleButtonShow = false;
    private boolean mImageLoaded = false;
    private TextView mNoLoadedText = null;
    /**
     * This boolean stand for permission granted and when activity resume
     * we can start the PhotoDownloadModal
     */
    private boolean mPostPermissionGranted = false;
    private int mSoftButtonBarSize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_fullscreen);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        mPhotoView = (PinchImageView) findViewById(R.id.photo_image);
        mPhotoControls = (RelativeLayout) findViewById(R.id.photo_controls);
        mInfoToolbar = (AppBarLayout) findViewById(R.id.info_toolbar);
        mShuffleButton = (FloatingActionButton) findViewById(R.id.shuffle_button);
        mURLButton = (FloatingActionButton) findViewById(R.id.url_button);
        mNoLoaded = (ImageView) findViewById(R.id.ic_download_iv);
        mNoLoadedText = (TextView) findViewById(R.id.error_text);

        mDescription = (TextView) findViewById(R.id.description);
        mUser = (TextView) findViewById(R.id.user);

        mPhotoControls.setVisibility(View.GONE);
        mInfoToolbar.setVisibility(View.GONE);
        mShuffleButton.setVisibility(View.GONE);
        mNoLoaded.setVisibility(View.GONE);
        mURLButton.setVisibility(View.GONE);

        if (getIntent() != null && getIntent().hasExtra(PHOTO_CONTENT)) {
            mContent = ((Photo) getIntent().getSerializableExtra(PHOTO_CONTENT));
            mapActivity(false);
        } else if (getIntent() != null && getIntent().hasExtra(RANDOM_CONTENT)) {
            shuffleNewPhoto();
        } else {
            finish();
        }

        findViewById(R.id.button_info).setOnClickListener(mInfoClick);
        findViewById(R.id.button_share).setOnClickListener(mShareClick);
        findViewById(R.id.button_download).setOnClickListener(mDownloadClick);

        mShuffleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shuffleNewPhoto();
            }
        });

        mVisible = true;
        mPhotoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleVisibilitytNavigationBar();
            }
        });

        isHideDisabled = PreferenceUtils.getInstance()
                .getBoolean(getString(R.string.settings_disable_fullscreen_key), true);
        mHideTiming = PreferenceUtils.getInstance().getString(
                getString(R.string.settings_timing_key), "1200ms");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mPostPermissionGranted = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPostPermissionGranted) {
            mPostPermissionGranted = false;
            Toast.makeText(this, "You can now download photos, set it as your amazing wallpaper " +
                            "or share with your friends!"
                    , Toast.LENGTH_LONG).show();
            PreferenceUtils.getInstance().setBoolean(PERMISSION_JUST_REQUIRED, true);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        mSoftButtonBarSize = ViewUtils.getSoftButtonsBarSizePort(this);
        setPortrait();
    }

    private void setLandscape() {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, 0, 0, 0);
        mPhotoControls.setLayoutParams(lp);

        RelativeLayout.LayoutParams lpFloatingsRand = new RelativeLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lpFloatingsRand.setMargins(0, ViewUtils.convertDpToPixel(-28f, this),
                mSoftButtonBarSize + ViewUtils.convertDpToPixel(16f, this),
                ViewUtils.convertDpToPixel(34f, this));
        lpFloatingsRand.addRule(RelativeLayout.ALIGN_PARENT_END);
        lpFloatingsRand.addRule(RelativeLayout.BELOW, R.id.info_toolbar);
        mShuffleButton.setLayoutParams(lpFloatingsRand);

        RelativeLayout.LayoutParams lpFloatingsUrl = new RelativeLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lpFloatingsUrl.setMargins(0, ViewUtils.convertDpToPixel(-20f, this),
                mSoftButtonBarSize + ViewUtils.convertDpToPixel(16f, this),
                0);
        lpFloatingsUrl.addRule(RelativeLayout.ALIGN_PARENT_END);
        lpFloatingsUrl.addRule(RelativeLayout.BELOW, R.id.shuffle_button);
        mURLButton.setLayoutParams(lpFloatingsUrl);
    }

    private void setPortrait() {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, 0, 0, mSoftButtonBarSize);
        mPhotoControls.setLayoutParams(lp);

        RelativeLayout.LayoutParams lpFloatingsRand = new RelativeLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lpFloatingsRand.setMargins(0, ViewUtils.convertDpToPixel(-28f, this),
                ViewUtils.convertDpToPixel(16f, this),
                ViewUtils.convertDpToPixel(34f, this));
        lpFloatingsRand.addRule(RelativeLayout.ALIGN_PARENT_END);
        lpFloatingsRand.addRule(RelativeLayout.BELOW, R.id.info_toolbar);
        mShuffleButton.setLayoutParams(lpFloatingsRand);

        RelativeLayout.LayoutParams lpFloatingsUrl = new RelativeLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lpFloatingsUrl.setMargins(0, ViewUtils.convertDpToPixel(-20f, this),
                ViewUtils.convertDpToPixel(16f, this),
                0);
        lpFloatingsUrl.addRule(RelativeLayout.ALIGN_PARENT_END);
        lpFloatingsUrl.addRule(RelativeLayout.BELOW, R.id.shuffle_button);
        mURLButton.setLayoutParams(lpFloatingsUrl);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == ORIENTATION_LANDSCAPE) {
            setLandscape();
        } else {
            setPortrait();
        }
    }

    private void toggleVisibilitytNavigationBar() {
        if (mVisible) {
            hideNavigationBar();
        } else {
            showNavigationBar();
        }
    }

    private void hideNavigationBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        ViewUtils.toFade(mPhotoControls, View.GONE);
        ViewUtils.toFade(mInfoToolbar, View.GONE);
        ViewUtils.toFade(mShuffleButton, View.GONE);
        ViewUtils.toFade(mURLButton, View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void showNavigationBar() {
        mPhotoView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    private void delayedHide() {
        String delayMillis = mHideTiming.replace("ms", "");
        int delayMillisInteger;
        try {
            delayMillisInteger = Integer.parseInt(delayMillis);
        } catch (Exception ex) {
            delayMillisInteger = 1200;
        }

        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillisInteger);
    }

    /**
     * Load a new photo from random api
     */
    public void shuffleNewPhoto() {
        mShuffleButtonShow = true;
        UnsplashAPI.getRandomPhoto(new DownloadPhotoInfoListListener() {
            @Override
            public void onSuccess(final Photo photoInfo) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mContent = photoInfo;
                        mapActivity(true);
                    }
                });
            }

            @Override
            public void onError() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mPhotoControls.setVisibility(View.GONE);
                        mInfoToolbar.setVisibility(View.GONE);
                        if (Build.VERSION.SDK_INT >= LOLLIPOP) {
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    findViewById(R.id.progress).setVisibility(View.GONE);
                                    ViewUtils.toFade(mNoLoaded, View.VISIBLE);
                                    ViewUtils.toFade(mNoLoadedText, View.VISIBLE);
                                    Drawable drawable = mNoLoaded.getDrawable();
                                    if (drawable instanceof Animatable) {
                                        ((Animatable) drawable).start();
                                    }
                                }
                            }, 2000);
                        } else {
                            findViewById(R.id.progress).setVisibility(View.GONE);
                            ViewUtils.toFade(mNoLoaded, View.VISIBLE);
                            ViewUtils.toFade(mNoLoadedText, View.VISIBLE);
                        }
                    }
                });
            }
        });
    }

    private void mapActivity(final boolean showShuffleButton) {
        /**
         * Analytics
         */
        Bundle bundle = new Bundle();
        bundle.putString("PhotoId", mContent.getId());
        bundle.putBoolean("RandomMode", showShuffleButton);
        App.mFirebaseAnalytics.logEvent("PhotoDetailView", bundle);

        mImageLoaded = false;
        GlideApp.with(this)
                .load(mContent.getUrls().getFull())
                .transition(withCrossFade())
                .fitCenter()
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        /**
                         * Analytics
                         */
                        Bundle bundle = new Bundle();
                        bundle.putString("PhotoId", mContent.getId());
                        App.mFirebaseAnalytics.logEvent("PhotoDetailViewError", bundle);

                        if (Build.VERSION.SDK_INT >= LOLLIPOP) {
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    findViewById(R.id.progress).setVisibility(View.GONE);
                                    ViewUtils.toFade(mNoLoaded, View.VISIBLE);
                                    ViewUtils.toFade(mNoLoadedText, View.VISIBLE);
                                    Drawable drawable = mNoLoaded.getDrawable();
                                    if (drawable instanceof Animatable) {
                                        ((Animatable) drawable).start();
                                    }
                                }
                            }, 2000);
                        } else {
                            findViewById(R.id.progress).setVisibility(View.GONE);
                            mNoLoaded.setVisibility(View.GONE);
                            ViewUtils.toFade(mNoLoadedText, View.VISIBLE);
                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        if (isHideDisabled) {
                            delayedHide();
                        }
                        mImageLoaded = true;
                        ViewUtils.toFade(mPhotoControls, View.VISIBLE);
                        ViewUtils.toFade(mShuffleButton, showShuffleButton ? View.VISIBLE : View.GONE);
                        ViewUtils.toFade(mInfoToolbar, View.VISIBLE);
                        ViewUtils.toFade(mURLButton, View.VISIBLE);
                        return false;
                    }
                })
                .into(mPhotoView);

        mDescription.setText(TextUtils.isEmpty(mContent.getDescription()) ?
                "No title" : mContent.getDescription());
        mUser.setText(mContent.getUser().getName());

        ViewUtils.toFade(mShuffleButton, showShuffleButton ? View.VISIBLE : View.GONE);
        ViewUtils.toFade(mInfoToolbar, View.VISIBLE);
        ViewUtils.toFade(mURLButton, View.VISIBLE);
        ViewUtils.toFade(mPhotoControls, View.GONE);

        if (mContent.getUser() != null
                && mContent.getUser().getUserLinks() != null
                && !TextUtils.isEmpty(mContent.getUser().getUserLinks().getHtml())
                && Uri.parse(mContent.getUser().getUserLinks().getHtml()) != null) {
            mURLButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent browse = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(mContent.getUser().getUserLinks().getHtml()));
                    startActivity(browse);
                }
            });
        } else {
            mURLButton.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            findViewById(R.id.progress).setVisibility(View.VISIBLE);
            mNoLoaded.setVisibility(View.GONE);
            mNoLoadedText.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= LOLLIPOP) {
                Drawable drawable = mNoLoaded.getDrawable();
                if (drawable instanceof Animatable) {
                    ((Animatable) drawable).stop();
                }
            }
        } catch (Exception ex) {
            //nothing to do!
        }
    }
}
