package it.josephbalzano.photoraw.view.custom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import it.josephbalzano.photoraw.R;
import it.josephbalzano.photoraw.util.StringUtils;

/**
 * Created by Joseph Balzano on 23/08/2017.
 */

public class InfoDialogFragment extends DialogFragment {
    private TextView mInfoMessage;
    private View mView;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        mView = inflater.inflate(R.layout.info_dialog, null);
        builder.setView(mView)
                .setPositiveButton("close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        InfoDialogFragment.this.getDialog().cancel();
                    }
                });

        mInfoMessage = (TextView) mView.findViewById(R.id.info_message);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("PhotoRaw is the app that was missing in your smartphone. With it you can see daily the most beautiful photos selected from Unsplash.com, download them and set them as screen saver. Simple and perfectly integrated with your smartphone (thanks to Material Design) allows you to discover new horizons, with the best photos of expert photographers and also view all their tricks (by reading the exif data). \n" +
                "Do you like a photo? Share it with anyone you want, and choose the most suitable sharing mode!\n\n");
        stringBuilder.append("Developed with ");
        stringBuilder.append(StringUtils.getEmojiByUnicode(0x2764));
        stringBuilder.append(" by Joseph Balzano\n");
        stringBuilder.append("Version: 1.0.0 (5)\n");
        mInfoMessage.setText(stringBuilder);
        return builder.create();
    }
}
