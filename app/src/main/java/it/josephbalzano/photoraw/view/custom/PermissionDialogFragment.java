package it.josephbalzano.photoraw.view.custom;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import it.josephbalzano.photoraw.R;
import it.josephbalzano.photoraw.util.StringUtils;

/**
 * Created by Joseph Balzano on 03/09/2017.
 */

public class PermissionDialogFragment extends DialogFragment {
    private TextView mInfoMessage;
    private View mView;
    private Activity mParentActivity = null;

    public void setParentActivity(Activity activity) {
        this.mParentActivity = activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        mView = inflater.inflate(R.layout.info_dialog, null);
        builder.setView(mView)
                .setPositiveButton("Ask me again " + StringUtils.getEmojiByUnicode(0x263A), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        ActivityCompat.requestPermissions(mParentActivity,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    }
                })
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        PermissionDialogFragment.this.getDialog().cancel();
                    }
                });

        mInfoMessage = (TextView) mView.findViewById(R.id.info_message);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Hey you!\n");
        stringBuilder.append("Why did you block my permissions? " + StringUtils.getEmojiByUnicode(0x1F625));
        stringBuilder.append("\nSo I can't download the image you selected");
        mInfoMessage.setText(stringBuilder);
        return builder.create();
    }
}
