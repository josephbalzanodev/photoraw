package it.josephbalzano.photoraw.view.custom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import it.josephbalzano.photoraw.R;

/**
 * Created by Joseph Balzano
 * Project: photoraw
 */

public class SearchBackgroundDialogFragment extends DialogFragment {
    private View mView = null;
    private View.OnClickListener mListener = null;

    public void setListener(View.OnClickListener onClickListener) {
        mListener = onClickListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        mView = inflater.inflate(R.layout.search_background_dialog, null);

        builder.setPositiveButton("set keyword", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mListener.onClick((EditText) mView.findViewById(R.id.edit_query));
            }
        });

        builder.setView(mView);
        return builder.create();
    }
}
