package it.josephbalzano.photoraw.view.custom;

import android.app.Activity;
import android.app.Dialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.text.TextUtils;
import android.view.View;

import java.text.SimpleDateFormat;

import it.josephbalzano.photoraw.R;
import it.josephbalzano.photoraw.api.DownloadPhotoInfoListListener;
import it.josephbalzano.photoraw.api.UnsplashAPI;
import it.josephbalzano.photoraw.model.Photo;
import it.josephbalzano.photoraw.util.ViewUtils;

/**
 * Created by Joseph Balzano on 18/08/2017.
 */

public class PhotoInfoModal extends BottomSheetDialogFragment {
    private Photo mPhoto = null;

    private View mView = null;
    private InfoItem mPhotoDateInfo;
    private InfoItem mPhotoExifInfo;
    private InfoItem mPhotoLocationInfo;
    private InfoItem mSocialViewsInfo;
    private InfoItem mSocialDownloadsInfo;
    private InfoItem mSocialLikesInfo;

    private boolean mJustLoaded = false;
    private boolean mCreated = false;

    private Activity mParentActivity;

    private DownloadPhotoInfoListListener mInfoListener = new DownloadPhotoInfoListListener() {
        @Override
        public void onSuccess(final Photo photoInfo) {
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (photoInfo == null) {
                        return;
                    } else {
                        mJustLoaded = true;
                    }
                    if (!mCreated) {
                        return;
                    }
                    mPhoto = photoInfo;

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
                    if (mPhoto.getCreatedDate() == null && mPhoto.getUpdateDate() == null) {
                        mPhotoDateInfo.setVisibility(View.GONE);
                    } else {
                        if (mPhoto.getCreatedDate() != null) {
                            mPhotoDateInfo.setTitle(String.format("Shot: %s",
                                    dateFormat.format(mPhoto.getCreatedDate())));
                        }
                        if (mPhoto.getUpdateDate() != null) {
                            mPhotoDateInfo.setDescription(String.format("Uploaded: %s",
                                    dateFormat.format(mPhoto.getUpdateDate())));
                        }
                    }

                    if (mPhoto.getExif() != null) {
                        mPhotoExifInfo.setTitle(String.format("%s", mPhoto.getExif().getModel()));
                        StringBuilder strBuilder = new StringBuilder();
                        if (!TextUtils.isEmpty(mPhoto.getExif().getExposureTime())) {
                            strBuilder.append(mPhoto.getExif().getExposureTime());
                            strBuilder.append("   ");
                        }
                        if (!TextUtils.isEmpty(mPhoto.getExif().getAperture())) {
                            strBuilder.append("f/" + mPhoto.getExif().getAperture());
                            strBuilder.append("   ");
                        }
                        if (!TextUtils.isEmpty(mPhoto.getExif().getFocalLength())) {
                            strBuilder.append(mPhoto.getExif().getFocalLength() + "mm");
                            strBuilder.append("   ");
                        }
                        if (mPhoto.getExif().getIso() != null) {
                            strBuilder.append("ISO" + mPhoto.getExif().getIso());
                        }
                        mPhotoExifInfo.setDescription(strBuilder.toString());
                    } else {
                        mPhotoExifInfo.setVisibility(View.GONE);
                    }

                    if (mPhoto.getLocation() != null) {
                        mPhotoLocationInfo.setTitle(String.format("%s", mPhoto.getLocation().getName()));
                        mPhotoLocationInfo.setDescription(String.format("%s", mPhoto.getLocation().getCountry()));
                    } else {
                        mPhotoLocationInfo.setVisibility(View.GONE);
                    }

                    // Social informations
                    mSocialLikesInfo.setTitle(String.format("%d", mPhoto.getLikes()));
                    mSocialDownloadsInfo.setTitle(String.format("%d", mPhoto.getDownloads()));
                    mSocialViewsInfo.setTitle(String.format("%d", mPhoto.getViews()));
                    ViewUtils.toFade(mView.findViewById(R.id.loading_view), View.GONE);
                }
            });
        }

        @Override
        public void onError() {

        }
    };

    public PhotoInfoModal() {
        super();
    }

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        mView = View.inflate(getContext(), R.layout.photo_info_modal, null);
        dialog.setContentView(mView);

        mPhotoDateInfo = (InfoItem) mView.findViewById(R.id.date_photo);
        mPhotoExifInfo = (InfoItem) mView.findViewById(R.id.exif_photo);
        mPhotoLocationInfo = (InfoItem) mView.findViewById(R.id.location_photo);

        mSocialViewsInfo = (InfoItem) mView.findViewById(R.id.view_photo);
        mSocialDownloadsInfo = (InfoItem) mView.findViewById(R.id.likes_photo);
        mSocialLikesInfo = (InfoItem) mView.findViewById(R.id.download_photo);
        mView.findViewById(R.id.loading_view).setVisibility(View.VISIBLE);

        mCreated = true;
        if (mJustLoaded) {
            mInfoListener.onSuccess(mPhoto);
        }
    }

    public void setPhoto(Photo photo) {
        if (photo == null) {
            return;
        }
        if (photo.isExtendedVersion()) {
            mPhoto = photo;
            mJustLoaded = true;
        } else {
            UnsplashAPI.getPhotoInfo(photo.getId(), mInfoListener);
        }
    }

    public void setParentActivity(Activity act) {
        mParentActivity = act;
    }
}
