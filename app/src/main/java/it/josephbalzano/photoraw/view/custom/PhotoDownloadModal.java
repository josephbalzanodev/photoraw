package it.josephbalzano.photoraw.view.custom;

import android.app.Activity;
import android.app.Dialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import it.josephbalzano.photoraw.R;
import it.josephbalzano.photoraw.model.Photo;
import it.josephbalzano.photoraw.util.DownloadUtils;

/**
 * Created by Joseph Balzano on 18/08/2017.
 */
public class PhotoDownloadModal extends BottomSheetDialogFragment {
    private Photo mPhoto = null;
    private Activity mActivityParent = null;

    public PhotoDownloadModal() {
        super();
    }

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        final View contentView = View.inflate(getContext(), R.layout.photo_download_modal, null);
        dialog.setContentView(contentView);

        if (mPhoto == null) {
            this.dismiss();
        }

        ((Spinner) contentView.findViewById(R.id.dimension)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String dimensionSelected = ((Spinner) contentView.findViewById(R.id.dimension))
                        .getSelectedItem().toString();

                switch (dimensionSelected) {
                    case "Raw":
                        ((TextView) contentView.findViewById(R.id.download_description))
                                .setText(mActivityParent.getString(R.string.raw_description));
                        break;
                    case "Big":
                        ((TextView) contentView.findViewById(R.id.download_description))
                                .setText(mActivityParent.getString(R.string.big_description));
                        break;
                    case "Medium":
                        ((TextView) contentView.findViewById(R.id.download_description))
                                .setText(mActivityParent.getString(R.string.medium_description));
                        break;
                    case "Small":
                        ((TextView) contentView.findViewById(R.id.download_description))
                                .setText(mActivityParent.getString(R.string.small_description));
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        contentView.findViewById(R.id.download_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dimensionSelected = ((Spinner) contentView.findViewById(R.id.dimension))
                        .getSelectedItem().toString();

                String urlToDownload = "";
                switch (dimensionSelected) {
                    case "Raw":
                        urlToDownload = mPhoto.getUrls().getRaw();
                        break;
                    case "Big":
                        urlToDownload = mPhoto.getUrls().getFull();
                        break;
                    case "Medium":
                        urlToDownload = mPhoto.getUrls().getRegular();
                        break;
                    case "Small":
                        urlToDownload = mPhoto.getUrls().getSmall();
                        break;
                }

                DownloadUtils.downloadImage(mActivityParent, urlToDownload, mPhoto.getId(),
                        TextUtils.isEmpty(mPhoto.getDescription()) ?
                                mPhoto.getUser().getUsername() : mPhoto.getDescription(),
                        mPhoto.getUser().getUsername());
            }
        });
    }

    public void setActivity(Activity activity) {
        mActivityParent = activity;
    }

    public void setPhoto(Photo photo) {
        mPhoto = photo;
    }
}
