package it.josephbalzano.photoraw.view.custom;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;

import it.josephbalzano.photoraw.R;
import it.josephbalzano.photoraw.model.Photo;
import it.josephbalzano.photoraw.util.DownloadUtils;
import it.josephbalzano.photoraw.util.PreferenceUtils;

import static it.josephbalzano.photoraw.App.PERMISSION_JUST_REQUIRED;

/**
 * Created by Joseph Balzano on 18/08/2017.
 */
public class PhotoShareModal extends BottomSheetDialogFragment {
    private Photo mPhoto = null;
    private Activity mParentActivity;

    public PhotoShareModal() {
        super();
    }

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        final View contentView = View.inflate(getContext(), R.layout.photo_share_modal, null);
        dialog.setContentView(contentView);

        contentView.findViewById(R.id.link_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, String.format("I've found this photo from %s: %s", mPhoto.getUser().getName(), mPhoto.getLinks().getHtml()));
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Share link"));
            }
        });

        contentView.findViewById(R.id.image_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (mParentActivity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        DownloadUtils.downloadImageAndShare(mParentActivity, mPhoto.getUrls().getRegular(), mPhoto.getId(),
                                TextUtils.isEmpty(mPhoto.getDescription()) ?
                                        mPhoto.getUser().getUsername() : mPhoto.getDescription(),
                                mPhoto.getUser().getUsername());
                    } else {
                        if (PreferenceUtils.getInstance().getBoolean(PERMISSION_JUST_REQUIRED, false)) {
                            PermissionDialogFragment dialogFragment = new PermissionDialogFragment();
                            dialogFragment.setParentActivity(mParentActivity);
                            dialogFragment.show(mParentActivity.getFragmentManager(), "downloadDialog");
                        } else {
                            ActivityCompat.requestPermissions(mParentActivity,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                        }
                    }
                } else {
                    DownloadUtils.downloadImageAndShare(mParentActivity, mPhoto.getUrls().getRegular(), mPhoto.getId(),
                            TextUtils.isEmpty(mPhoto.getDescription()) ?
                                    mPhoto.getUser().getUsername() : mPhoto.getDescription(),
                            mPhoto.getUser().getUsername());
                }
            }
        });

        contentView.findViewById(R.id.wallpaper_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (mParentActivity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        DownloadUtils.downloadImageAndWallpaperize(mParentActivity, mPhoto.getUrls().getRegular(), mPhoto.getId(),
                                TextUtils.isEmpty(mPhoto.getDescription()) ?
                                        mPhoto.getUser().getUsername() : mPhoto.getDescription(),
                                mPhoto.getUser().getUsername());
                    } else {
                        if (PreferenceUtils.getInstance().getBoolean(PERMISSION_JUST_REQUIRED, false)) {
                            PermissionDialogFragment dialogFragment = new PermissionDialogFragment();
                            dialogFragment.setParentActivity(mParentActivity);
                            dialogFragment.show(mParentActivity.getFragmentManager(), "downloadDialog");
                        } else {
                            ActivityCompat.requestPermissions(mParentActivity,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                        }
                    }
                } else {
                    DownloadUtils.downloadImageAndWallpaperize(mParentActivity, mPhoto.getUrls().getRegular(), mPhoto.getId(),
                            TextUtils.isEmpty(mPhoto.getDescription()) ?
                                    mPhoto.getUser().getUsername() : mPhoto.getDescription(),
                            mPhoto.getUser().getUsername());
                }
            }
        });

        if (mPhoto == null) {
            this.dismiss();
        }
    }

    public void setPhoto(Photo photo) {
        mPhoto = photo;
    }

    public void setParentActivity(Activity activity) {
        mParentActivity = activity;
    }
}
