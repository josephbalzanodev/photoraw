package it.josephbalzano.photoraw.view.custom;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.xw.repo.BubbleSeekBar;

import java.util.ArrayList;
import java.util.Arrays;

import it.josephbalzano.photoraw.R;
import it.josephbalzano.photoraw.service.WallpaperBackgroundService;
import it.josephbalzano.photoraw.util.PreferenceUtils;
import it.josephbalzano.photoraw.util.StringUtils;
import it.josephbalzano.photoraw.util.ViewUtils;

/**
 * Created by Joseph Balzano (joseph.balzano@piksel.com)
 * for Mediaset S.p.A. on 19/09/2017.
 * Project: photoraw
 */

public class BackgroundDialogFragment extends DialogFragment {
    private static final String TAG = "BackgroundDialogFragmen";

    private TextView mInfoMessage;
    private View mView;
    private Activity mParent = null;
    private BubbleSeekBar mSeek = null;
    private Intent mAlarmIntent = null;
    private PendingIntent mPendingIntent = null;
    private AlarmManager mAlarmManager = null;
    private Spinner mSpinn = null;
    private ArrayAdapter<String> mAdapter = null;

    public void setParentActivity(Activity activity) {
        this.mParent = activity;
        mAlarmIntent = new Intent(mParent, WallpaperBackgroundService.class);
        mAlarmIntent.putExtra("src",
                PreferenceUtils.getInstance().getString("backgroundServiceSrc", null));
        mPendingIntent = PendingIntent
                .getBroadcast(mParent.getBaseContext(), 0, mAlarmIntent, 0);
        mAlarmManager = (AlarmManager) mParent.getSystemService(Context.ALARM_SERVICE);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        mView = inflater.inflate(R.layout.background_dialog, null);
        mSeek = (BubbleSeekBar) mView.findViewById(R.id.seek_bar);
        mSpinn = (Spinner) mView.findViewById(R.id.spinner_src);

        String[] arraySpinner = new String[]{
                "Random", "Lastest", "from search..."
        };
        ArrayList<String> arraySpinnerList = new ArrayList<String>(Arrays.asList(arraySpinner));
        mAdapter = new ArrayAdapter<String>(mParent.getBaseContext(),
                android.R.layout.simple_spinner_dropdown_item, arraySpinnerList);
        mSpinn.setAdapter(mAdapter);

        mSpinn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String items = mSpinn.getSelectedItem().toString();
                if (items.equals("from search...")) {
                    SearchBackgroundDialogFragment dialog = new SearchBackgroundDialogFragment();
                    dialog.setCancelable(false);
                    dialog.setListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!TextUtils.isEmpty(((EditText) view)
                                    .getText().toString())) {
                                mAdapter.remove("from search...");
                                mAdapter.add(((EditText) view).getText().toString());
                                mAdapter.add("from search...");
                                mAdapter.notifyDataSetChanged();
                                mSpinn.setSelection(mAdapter.getPosition(((EditText) view).getText().toString()), true);
                            } else {
                                Toast.makeText(mView.getContext(), "You can't leave blank search",
                                        Toast.LENGTH_SHORT).show();
                                mSpinn.setSelection(0, true);
                            }
                        }
                    });
                    dialog.show(getFragmentManager(), "search_background");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mView.findViewById(R.id.stop_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAlarmManager.cancel(mPendingIntent);

                ViewUtils.toFade(mView.findViewById(R.id.warning_message), View.GONE);
                PreferenceUtils.getInstance().setBoolean("backgroundService", false);
            }
        });

        builder.setView(mView)
                .setPositiveButton("start service", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        int interval = mSeek.getProgress() * 60 * 60 * 1000;

                        Intent newAlarmIntent = new Intent(mParent, WallpaperBackgroundService.class);
                        newAlarmIntent.putExtra("src", mSpinn.getSelectedItem().toString());
                        PreferenceUtils.getInstance().setString("backgroundServiceSrc",
                                mSpinn.getSelectedItem().toString());
                        PendingIntent newPendingIntent = PendingIntent
                                .getBroadcast(mParent.getBaseContext(), 0, newAlarmIntent, 0);

                        if (PreferenceUtils.getInstance().getBoolean("backgroundService", false)) {
                            mAlarmManager.cancel(newPendingIntent);
                        } else {
                            PreferenceUtils.getInstance().setBoolean("backgroundService", true);
                        }

                        mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                                System.currentTimeMillis(), interval, mPendingIntent);

                        WallpaperBackgroundService.startDownload(mParent.getApplicationContext(), newAlarmIntent);

                        Toast.makeText(mParent, "The wallpaper service it's starting " + StringUtils.getEmojiByUnicode(0x1F4F8),
                                Toast.LENGTH_LONG).show();

                    }
                });

        if (PreferenceUtils.getInstance().getBoolean("backgroundService", false)) {
            mView.findViewById(R.id.warning_message).setVisibility(View.VISIBLE);
        } else {
            mView.findViewById(R.id.warning_message).setVisibility(View.GONE);
        }
        return builder.create();
    }
}

