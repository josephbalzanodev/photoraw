package it.josephbalzano.photoraw.util;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import it.josephbalzano.photoraw.App;
import it.josephbalzano.photoraw.view.custom.PermissionDialogFragment;

import static android.content.ContentValues.TAG;

/**
 * Created by Joseph Balzano on 21/08/2017.
 */

public class DownloadUtils {
    private static DownloadManager mDownloadManager = null;

    private static BroadcastReceiver downloadImage(final Activity activity, String url, String title,
                                                   String fileName, final boolean thenShare,
                                                   final boolean thenWallpaperize) {
        final long downloadId;
        try {
            mDownloadManager = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
            if (mDownloadManager == null) {
                return null;
            }
        } catch (Exception ex) {
            return null;
        }
        Uri uri = Uri.parse(url);

        Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                .mkdirs();


        try {
            downloadId = mDownloadManager.enqueue(new DownloadManager.Request(uri)
                    .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI |
                            DownloadManager.Request.NETWORK_MOBILE)
                    .setAllowedOverRoaming(false)
                    .setTitle("PhotoRaw downloading")
                    .setDescription("I'm downloading the selected photo from Unsplash.com")
                    .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
                            fileName + ".jpg"));

            BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                        long downloadedId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                        if (downloadId == downloadedId) {
                            Bundle extras = intent.getExtras();
                            DownloadManager.Query q = new DownloadManager.Query();
                            q.setFilterById(extras.getLong(DownloadManager.EXTRA_DOWNLOAD_ID));
                            Cursor c = mDownloadManager.query(q);
                            if (c.moveToFirst()) {
                                int status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
                                if (status == DownloadManager.STATUS_SUCCESSFUL) {
                                    final String uri = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                                    if (thenShare) {
                                        Intent sendIntent = new Intent(Intent.ACTION_SEND);
                                        sendIntent.setType("image/*");
                                        sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(uri));
                                        sendIntent.putExtra(Intent.EXTRA_TEXT, "Ehi, I've found this amazing photo on PhotoRaw App! Download it by Google Play to see the best photos from around the world: https://play.google.com/store/apps/details?id=it.josephbalzano.photoraw");
                                        activity.startActivity(Intent.createChooser(sendIntent, "Share Image"));
                                    } else if (thenWallpaperize) {
                                        try {
                                            WallpaperManager myWallpaperManager
                                                    = WallpaperManager.getInstance(activity.getApplicationContext());
                                            Uri newPhotoUri = Uri.parse(uri);
                                            if (newPhotoUri != null) {
                                                File photoFile = new File(newPhotoUri.getPath());
                                                Uri contentURI = getImageContentUri(context, photoFile.getAbsolutePath());
                                                Intent goToWallpaper = new Intent(myWallpaperManager
                                                        .getCropAndSetWallpaperIntent(contentURI));
                                                activity.startActivity(goToWallpaper);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        Toast.makeText(context, "Image downloaded successfully",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                            activity.unregisterReceiver(this);
                            App.mBroadcastReciverRegistered.remove(this);
                        }
                    }
                }
            };
            activity.registerReceiver(downloadReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
            App.mBroadcastReciverRegistered.put(activity, downloadReceiver);
            return downloadReceiver;
        } catch (SecurityException ex) {
            PermissionDialogFragment dialogFragment = new PermissionDialogFragment();
            dialogFragment.setParentActivity(activity);
            dialogFragment.show(activity.getFragmentManager(), "downloadDialog");
            return null;
        }
    }

    public static void downloadImage(Activity activity, String url, String id, String title, String fileName) {
        downloadImage(activity, url, title, fileName, false, false);

        /**
         * Analytics
         */
        Bundle bundle = new Bundle();
        bundle.putString("PhotoId", id);
        App.mFirebaseAnalytics.logEvent("DownloadImage", bundle);
    }

    public static void downloadImageAndShare(Activity activity, String url, String id, String title, String fileName) {
        downloadImage(activity, url, title, fileName, true, false);

        /**
         * Analytics
         */
        Bundle bundle = new Bundle();
        bundle.putString("PhotoId", id);
        App.mFirebaseAnalytics.logEvent("DownloadAndShareImage", bundle);
    }

    public static void downloadImageAndWallpaperize(Activity activity, String url,String id, String title, String fileName) {
        downloadImage(activity, url, title, fileName, false, true);

        /**
         * Analytics
         */
        Bundle bundle = new Bundle();
        bundle.putString("PhotoId", id);
        App.mFirebaseAnalytics.logEvent("DownloadAndWallpaperizeImage", bundle);
    }

    public static Uri getLocalBitmapUri(Context ctx, Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file = new File(ctx.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public static Uri getImageContentUri(Context context, String absPath) {
        Log.v(TAG, "getImageContentUri: " + absPath);

        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                , new String[]{MediaStore.Images.Media._ID}
                , MediaStore.Images.Media.DATA + "=? "
                , new String[]{absPath}, null);

        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            return Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Integer.toString(id));

        } else if (!absPath.isEmpty()) {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.DATA, absPath);
            return context.getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        } else {
            return null;
        }
    }
}
