package it.josephbalzano.photoraw.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import it.josephbalzano.photoraw.model.HintPreferences;

/**
 * Created by Joseph Balzano on 24/08/2017.
 */

public class PreferenceUtils {
    private static final String TAG = "PreferenceUtils";

    private static PreferenceUtils mInstance;
    private static Context mCtx;
    private static SharedPreferences mPrefs = null;

    public PreferenceUtils() {
        super();
    }

    public static void init(Context ctx) {
        mInstance = new PreferenceUtils();
        mCtx = ctx;
        mPrefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
    }

    public static PreferenceUtils getInstance() {
        return mInstance;
    }

    public String getString(String key, String defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
        return prefs.getString(key, defaultValue);
    }

    public void setString(String key, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
        editor.commit();
    }

    public void setBoolean(String key, boolean value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.apply();
        editor.commit();
    }

    public Set<String> getArrayString(String key, Set<String> defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
        return prefs.getStringSet(key, defaultValue);
    }

    public void setArrayString(String key, Set<String> values) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putStringSet(key, values);
        editor.apply();
        editor.commit();
    }

    public void addToArrayString(String key, Set<String> values) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
        Set<String> oldSet = getArrayString(key, new android.support.v4.util.ArraySet<String>());
        oldSet.addAll(values);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(key);
        editor.apply();
        editor.commit();
        editor.putStringSet(key, oldSet);
        editor.apply();
        editor.commit();
    }

    public void addToArrayString(String key, String... values) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
        Set<String> oldSet = getArrayString(key, new android.support.v4.util.ArraySet<String>());
        for (String item : values) {
            oldSet.add(item);
        }
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(key);
        editor.apply();
        editor.commit();
        editor.putStringSet(key, oldSet);
        editor.commit();
    }

    public void removeFromArrayString(String key, Set<String> values) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
        Set<String> oldSet = getArrayString(key, new android.support.v4.util.ArraySet<String>());
        for (String item : values) {
            if (oldSet.contains(item)) {
                oldSet.remove(item);
            }
        }
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(key);
        editor.apply();
        editor.commit();
        editor.putStringSet(key, oldSet);
        editor.apply();
        editor.commit();
    }

    public void removeFromArrayString(String key, String... values) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
        Set<String> oldSet = getArrayString(key, new android.support.v4.util.ArraySet<String>());
        for (String item : values) {
            if (oldSet.contains(item)) {
                oldSet.remove(item);
            }
        }
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(key);
        editor.commit();
        editor.putStringSet(key, oldSet);
        editor.commit();
    }

    @Nullable
    public HintPreferences getHintPreference(String key, @Nullable HintPreferences defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
        String jsonHint = "";
        Gson gson = new Gson();
        if (defaultValue != null) {
            try {
                jsonHint = gson.toJson(defaultValue);
            } catch (Exception ex) {
                Log.d(TAG, "Exception parse to json");
                jsonHint = "";
            }
        }
        String savedHint = prefs.getString(key, jsonHint);
        if (!TextUtils.isEmpty(savedHint)) {
            try {
                return gson.fromJson(savedHint, HintPreferences.class);
            } catch (Exception ex) {
                Log.d(TAG, "Exception parse from json to hintPreference");
                return null;
            }
        } else {
            return null;
        }
    }

    public void setHintPreference(String key, @NonNull HintPreferences value) {
        String jsonHint = "";
        Gson gson = new Gson();
        if (value != null) {
            try {
                jsonHint = gson.toJson(value);
            } catch (Exception ex) {
                Log.d(TAG, "Exception parse to json - HintPreference not saved");
                return;
            }
        } else {
            Log.d(TAG, "Value is null - HintPreference not saved");
            return;
        }
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(key, jsonHint);
        editor.apply();
        editor.commit();
    }

    public void addToHintPreference(String key, List<String> values) {
        if (values == null || values.size() == 0) {
            Log.e(TAG, "addToHintPreference() values is null or empty");
            return;
        }

        HintPreferences oldValues = getHintPreference(key, null);

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.remove(key);
        editor.apply();
        editor.commit();

        if (oldValues != null && oldValues.getHints() != null) {
            oldValues.getHints().addAll(values);
        } else {
            setHintPreference(key, new HintPreferences().setHints(values));
            Log.e(TAG, "addToHintPreference() values added successfully");
            return;
        }

        setHintPreference(key, oldValues);
        Log.e(TAG, "addToHintPreference(String...) values added successfully");
    }

    public void addToHintPreference(String key, String... values) {
        if (values == null || values.length == 0) {
            Log.e(TAG, "addToHintPreference(String...) values is null or empty");
            return;
        }

        HintPreferences oldValues = getHintPreference(key, null);

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.remove(key);
        editor.apply();
        editor.commit();

        if (oldValues != null && oldValues.getHints() != null) {
            for (String item : values) {
                if (!oldValues.getHints().contains(item)) {
                    oldValues.getHints().add(item);
                }
            }
        } else {
            List<String> newValues = new ArrayList<>();
            for (String item : values) {
                newValues.add(item);
            }
            setHintPreference(key, new HintPreferences().setHints(newValues));
            Log.e(TAG, "addToHintPreference(String...) values added successfully");
            return;
        }

        setHintPreference(key, oldValues);
        Log.e(TAG, "addToHintPreference(String...) values added successfully");
    }

    public void removeFromHintPreference(String key, Set<String> values) {
        if (values == null || values.size() == 0) {
            Log.e(TAG, "removeFromHintPreference() values is null or empty");
            return;
        }

        HintPreferences oldValues = getHintPreference(key, null);

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.remove(key);
        editor.apply();
        editor.commit();

        if (oldValues != null && oldValues.getHints() != null) {
            oldValues.getHints().removeAll(values);
        } else {
            Log.e(TAG, "removeFromHintPreference() removed key and values");
            return;
        }

        setHintPreference(key, oldValues);
        Log.e(TAG, "removeFromHintPreference() values removed successfully");
    }

    public void removeFromHintPreference(String key, String... values) {
        if (values == null || values.length == 0) {
            Log.e(TAG, "removeFromHintPreference(String...) values is null or empty");
            return;
        }

        HintPreferences oldValues = getHintPreference(key, null);

        removePreference(key);

        if (oldValues != null && oldValues.getHints() != null) {
            for (String item : values) {
                oldValues.getHints().remove(item);
            }
        } else {
            Log.e(TAG, "removeFromHintPreference() removed key and values");
        }

        if (oldValues.getHints() != null
                && oldValues.getHints().size() > 0) {
            setHintPreference(key, oldValues);
        } else {
            removePreference(key);
        }
        Log.e(TAG, "removeFromHintPreference() values removed successfully");
    }

    public void removePreference(String key) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.remove(key);
        editor.apply();
        editor.commit();
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
        return prefs.getBoolean(key, defaultValue);
    }

    public int getInt(String key, int defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
        return prefs.getInt(key, defaultValue);
    }
}
