package it.josephbalzano.photoraw.util;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Joseph Balzano on 23/08/2017.
 */

public class StringUtils {
    public static String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }

    // Unsplash format: yyyy-MM-dd'T'HH:mm:ssXXX
    public static Date getDateObjFromString(String dateStr, String format) {
        Date dateReturn = null;
        try {
            if (!TextUtils.isEmpty(dateStr)) {
                SimpleDateFormat dateFormat = new SimpleDateFormat(format);
                dateReturn = dateFormat.parse(dateStr);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateReturn;
    }
}
