package it.josephbalzano.photoraw.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/**
 * Created by Joseph Balzano on 18/08/2017.
 */

public class ViewUtils {
    public static void toFade(View component, int visibility) {
        if (visibility != VISIBLE
                && visibility != INVISIBLE
                && visibility != GONE
                || component == null
                || visibility == component.getVisibility()) {
            return;
        }
        AlphaAnimation anim;
        switch (visibility) {
            case VISIBLE:
                anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(200);
                break;
            case INVISIBLE:
            case GONE:
                anim = new AlphaAnimation(1.0f, 0.0f);
                anim.setDuration(200);
                break;
            default:
                anim = new AlphaAnimation(0.0f, 0.0f);
                anim.setDuration(0);
        }
        try {
            component.startAnimation(anim);
            component.setVisibility(visibility);
        } catch (Exception ex) {
            Log.e("ViewUtils", "Called from wrong thread.");
        }
    }

    @RequiresApi(17)
    public static int getSoftButtonsBarSizePort(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int usableHeight = metrics.heightPixels;
            activity.getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
            int realHeight = metrics.heightPixels;
            if (realHeight > usableHeight)
                return realHeight - usableHeight;
            else
                return 0;
        }
        return 0;
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static int convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }
}
