package it.josephbalzano.photoraw.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.network.connectionclass.ConnectionClassManager;
import com.facebook.network.connectionclass.ConnectionQuality;

import java.util.ArrayList;
import java.util.List;

import it.josephbalzano.photoraw.R;
import it.josephbalzano.photoraw.manager.GlideApp;
import it.josephbalzano.photoraw.model.Photo;
import it.josephbalzano.photoraw.util.DownloadUtils;
import it.josephbalzano.photoraw.util.PreferenceUtils;
import it.josephbalzano.photoraw.view.activity.PhotoFullscreenActivity;
import it.josephbalzano.photoraw.view.custom.PermissionDialogFragment;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static it.josephbalzano.photoraw.App.PERMISSION_JUST_REQUIRED;
import static it.josephbalzano.photoraw.view.activity.PhotoFullscreenActivity.PHOTO_CONTENT;

/**
 * Created by Joseph Balzano on 17/08/2017.
 */

public class CuratedPhotoListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "CuratedPhotoListAdapter";

    private static final String POPULARPEOPLE = "popular_people_container";
    private static final String LOADING = "loading_container";
    private static final int POPULARPEOPLE_VIEW_TYPE = 0;
    private static final int PHOTO_VIEW_TYPE = 1;
    private static final int LOADING_VIEW_TYPE = 2;

    private List<Photo> mRenderCuratedPhoto = new ArrayList<>();
    private List<Photo> mOriginalCuratedPhoto = new ArrayList<>();
    private Activity mParentActivity = null;

    public CuratedPhotoListAdapter(List<Photo> curatedPhoto, boolean showPopularUsers,
                                   Activity activity, boolean insertLoading) {
        if (showPopularUsers) {
            Photo fakePhoto = new Photo();
            fakePhoto.setId(POPULARPEOPLE);
            this.mRenderCuratedPhoto.add(0, fakePhoto);
        }
        this.mRenderCuratedPhoto.addAll(curatedPhoto);
        this.mOriginalCuratedPhoto.addAll(curatedPhoto);
        this.mParentActivity = activity;

        if (insertLoading) {
            Photo fakePhotoLoading = new Photo();
            fakePhotoLoading.setId(LOADING);
            this.mRenderCuratedPhoto.add(fakePhotoLoading);
        }
    }

    public void addPhoto(List<Photo> curatedPhoto) {
        boolean hasLoading = false;
        if (this.mRenderCuratedPhoto.get(mRenderCuratedPhoto.size() - 1).getId() == LOADING) {
            this.mRenderCuratedPhoto.remove(mRenderCuratedPhoto.size() - 1);
            hasLoading = true;
        }
        this.mRenderCuratedPhoto.addAll(curatedPhoto);
        this.mOriginalCuratedPhoto.addAll(curatedPhoto);

        if (hasLoading) {
            Photo fakePhotoLoading = new Photo();
            fakePhotoLoading.setId(LOADING);
            this.mRenderCuratedPhoto.add(fakePhotoLoading);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == PHOTO_VIEW_TYPE) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.main_image_item, parent, false);

            return new PhotoHolder(itemView);
        } else if (viewType == POPULARPEOPLE_VIEW_TYPE) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.main_popular_people_container, parent, false);

            return new PopularPeopleHolder(itemView);
        } else if (viewType == LOADING_VIEW_TYPE) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.infinite_scroll_loading, parent, false);

            return new LoadingHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PhotoHolder) {
            ((PhotoHolder) holder).bind(mRenderCuratedPhoto.get(position));
        } else if (holder instanceof PopularPeopleHolder) {
            ((PopularPeopleHolder) holder).bind(mOriginalCuratedPhoto);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!TextUtils.isEmpty(mRenderCuratedPhoto.get(position).getId())) {
            switch (mRenderCuratedPhoto.get(position).getId()) {
                case POPULARPEOPLE:
                    return POPULARPEOPLE_VIEW_TYPE;
                case LOADING:
                    return LOADING_VIEW_TYPE;
                default:
                    return PHOTO_VIEW_TYPE;
            }
        } else {
            return PHOTO_VIEW_TYPE;
        }
    }

    @Override
    public int getItemCount() {
        return mRenderCuratedPhoto != null ? mRenderCuratedPhoto.size() : 0;
    }

    public void removeLoading() {
        if (this.mRenderCuratedPhoto.get(mRenderCuratedPhoto.size() - 1).getId() == LOADING) {
            this.mRenderCuratedPhoto.remove(mRenderCuratedPhoto.size() - 1);
        }
    }

    public class PhotoHolder extends RecyclerView.ViewHolder {
        private final ImageView mImage;
        private final ImageView mDownloadButton;
        private final ImageView mWallpaperButton;
        private Photo mPhoto = null;

        PhotoHolder(View view) {
            super(view);
            mImage = (ImageView) view.findViewById(R.id.image);
            mDownloadButton = (ImageView) view.findViewById(R.id.download_button);
            mWallpaperButton = (ImageView) view.findViewById(R.id.wallpaperize_button);
        }

        public void bind(final Photo photo) {
            mPhoto = photo;
            ConnectionQuality connectionQuality = ConnectionClassManager.getInstance()
                    .getCurrentBandwidthQuality();
            String imageUrl = null;
            switch (connectionQuality) {
                case UNKNOWN:
                case POOR:
                    imageUrl = photo.getUrls().getSmall();
                    break;
                case MODERATE:
                    imageUrl = photo.getUrls().getRegular();
                    break;
                case GOOD:
                case EXCELLENT:
                    imageUrl = photo.getUrls().getFull();
                    break;
            }
            GlideApp.with(itemView.getContext())
                    .load(imageUrl)
                    .placeholder(R.drawable.placeholder)
                    .transition(withCrossFade())
                    .into(mImage);

            mImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent goFullscreen = new Intent((AppCompatActivity) itemView.getContext(), PhotoFullscreenActivity.class);
                    goFullscreen.putExtra(PHOTO_CONTENT, photo);

                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(mParentActivity, (View) mImage, "image");
                    itemView.getContext().startActivity(goFullscreen, options.toBundle());
                }
            });

            mDownloadButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        if (mParentActivity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED) {
                            Toast.makeText(mParentActivity, "Download started", Toast.LENGTH_SHORT).show();
                            downloadImage(mPhoto);
                        } else {
                            if (PreferenceUtils.getInstance().getBoolean(PERMISSION_JUST_REQUIRED, false)) {
                                PermissionDialogFragment dialogFragment = new PermissionDialogFragment();
                                dialogFragment.setParentActivity(mParentActivity);
                                dialogFragment.show(mParentActivity.getFragmentManager(), "downloadDialog");
                            } else {
                                ActivityCompat.requestPermissions(mParentActivity,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                            }
                        }
                    } else {
                        Toast.makeText(mParentActivity, "Download started", Toast.LENGTH_SHORT).show();
                        downloadImage(mPhoto);
                    }
                }
            });

            mWallpaperButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        if (mParentActivity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED) {
                            Toast.makeText(mParentActivity, "Download started for set wallpaper", Toast.LENGTH_SHORT).show();
                            DownloadUtils.downloadImageAndWallpaperize(mParentActivity, photo.getUrls().getFull(), photo.getId(),
                                    TextUtils.isEmpty(photo.getDescription()) ?
                                            photo.getUser().getUsername() : photo.getDescription(),
                                    photo.getUser().getUsername());
                        } else {
                            if (PreferenceUtils.getInstance().getBoolean(PERMISSION_JUST_REQUIRED, false)) {
                                PermissionDialogFragment dialogFragment = new PermissionDialogFragment();
                                dialogFragment.setParentActivity(mParentActivity);
                                dialogFragment.show(mParentActivity.getFragmentManager(), "downloadDialog");
                            } else {
                                ActivityCompat.requestPermissions(mParentActivity,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                            }
                        }
                    } else {
                        Toast.makeText(mParentActivity, "Download started for set wallpaper", Toast.LENGTH_SHORT).show();
                        DownloadUtils.downloadImageAndWallpaperize(mParentActivity, photo.getUrls().getFull(),photo.getId(),
                                TextUtils.isEmpty(photo.getDescription()) ?
                                        photo.getUser().getUsername() : photo.getDescription(),
                                photo.getUser().getUsername());
                    }
                }
            });

            /*if (PreferenceUtils.getInstance().getBoolean(
                    mParentActivity.getString(R.string.tutorial_key_wallpaper_from_home), false)) {
                PreferenceUtils.getInstance().removePreference(
                        mParentActivity.getString(R.string.tutorial_key_wallpaper_from_home));

                new TapTargetSequence(mParentActivity)
                        .targets(TapTarget.forView(mWallpaperButton, "Wallpaperize",
                                "With this button you can download image and set it as you amazing wallpaper"),
                                TapTarget.forView(mDownloadButton, "Download Button", "" +
                                        "or, with this, you can only download image.\n\nResolution of image can set on Settings"));
            }*/
        }

        private void downloadImage(Photo photo) {
            String dimensionSelected = PreferenceUtils.getInstance()
                    .getString(mParentActivity.getString(R.string.settings_download_key),
                            mParentActivity.getString(R.string.settings_download_default_value));

            String urlToDownload = "";
            switch (dimensionSelected) {
                case "Raw":
                    urlToDownload = photo.getUrls().getRaw();
                    break;
                case "Big":
                    urlToDownload = photo.getUrls().getFull();
                    break;
                case "Medium":
                    urlToDownload = photo.getUrls().getRegular();
                    break;
                case "Small":
                    urlToDownload = photo.getUrls().getSmall();
                    break;
            }

            DownloadUtils.downloadImage(mParentActivity, urlToDownload, photo.getId(),
                    TextUtils.isEmpty(photo.getDescription()) ?
                            photo.getUser().getUsername() : photo.getDescription(),
                    photo.getUser().getUsername());
        }
    }

    public class PopularPeopleHolder extends RecyclerView.ViewHolder {
        private final RecyclerView mPeopleList;
        private final LinearLayoutManager mCuratedPeopleListLayoutManager;
        private CuratedPeopleListAdapter mCuratedPeopleAdapter;

        PopularPeopleHolder(View itemView) {
            super(itemView);
            mPeopleList = (RecyclerView) itemView.findViewById(R.id.people_list);

            mCuratedPeopleListLayoutManager
                    = new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false);
            mPeopleList.setLayoutManager(mCuratedPeopleListLayoutManager);
            mPeopleList.setItemAnimator(new DefaultItemAnimator());
        }

        public void bind(List<Photo> photoList) {
            mCuratedPeopleAdapter = new CuratedPeopleListAdapter(photoList);
            mPeopleList.setAdapter(mCuratedPeopleAdapter);
        }
    }

    public class LoadingHolder extends RecyclerView.ViewHolder {
        LoadingHolder(View itemView) {
            super(itemView);
        }
    }
}
