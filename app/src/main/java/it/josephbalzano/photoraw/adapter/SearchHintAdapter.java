package it.josephbalzano.photoraw.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import it.josephbalzano.photoraw.R;
import it.josephbalzano.photoraw.model.HintPreferences;
import it.josephbalzano.photoraw.util.PreferenceUtils;
import it.josephbalzano.photoraw.view.fragment.SearchHintFragment;

/**
 * Created by Joseph Balzano on 28/08/2017.
 */
public class SearchHintAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String HINT_PREFERENCE_KEY = "hint_search";
    public static final String FAKE_ITEM = "<fake_item>";
    private static final int EMPTY_ITEM_VIEW = 0;
    private static final int STRING_ITEM_VIEW = 1;
    private SearchHintFragment.OnSearchFromHint mListener;

    private List<String> mHint = new ArrayList<>();

    public SearchHintAdapter(SearchHintFragment.OnSearchFromHint listener) {
        super();
        mListener = listener;
        HintPreferences hints = PreferenceUtils.getInstance()
                .getHintPreference(HINT_PREFERENCE_KEY, new HintPreferences());
        if (hints != null && hints.getHints() != null && hints.getHints().size() > 0) {
            mHint.addAll(hints.getHints());
        }
        if (mHint.size() == 0) {
            mHint.add(FAKE_ITEM);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == EMPTY_ITEM_VIEW) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.hint_serarch_empty_element, parent, false);

            return new FakeItemHolder(itemView);
        } else if (viewType == STRING_ITEM_VIEW) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.hint_search_string_element, parent, false);

            return new StringItemHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof StringItemHolder) {
            ((StringItemHolder) holder).bind(mHint.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mHint != null ? mHint.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        switch (mHint.get(position)) {
            case FAKE_ITEM:
                return EMPTY_ITEM_VIEW;
            default:
                return STRING_ITEM_VIEW;
        }
    }

    public void removeItem(int position) {
        PreferenceUtils.getInstance().removeFromHintPreference(HINT_PREFERENCE_KEY, mHint.get(position));
        mHint.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mHint.size());

        if (mHint.size() == 0) {
            mHint.add(FAKE_ITEM);

            notifyItemInserted(0);
        }
    }

    public class FakeItemHolder extends RecyclerView.ViewHolder {
        public FakeItemHolder(View itemView) {
            super(itemView);
        }
    }

    public class StringItemHolder extends RecyclerView.ViewHolder {
        private final TextView mLabel;

        public StringItemHolder(View itemView) {
            super(itemView);

            mLabel = (TextView) itemView.findViewById(R.id.search_label);
        }

        public void bind(final String str) {
            mLabel.setText(str);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onClickOnHint(str);
                }
            });
        }
    }
}
