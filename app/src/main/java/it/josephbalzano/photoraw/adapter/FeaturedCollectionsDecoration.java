package it.josephbalzano.photoraw.adapter;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Joseph Balzano on 31/08/2017.
 */

public class FeaturedCollectionsDecoration extends RecyclerView.ItemDecoration {
    private int mSpace;
    private int mNCollection;

    public FeaturedCollectionsDecoration(int space, int nCollections) {
        this.mSpace = space;
        this.mNCollection = nCollections;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        outRect.left = mSpace;
        outRect.right = mSpace;
        outRect.bottom = 0;
        outRect.top = mSpace;

        if (parent.getChildLayoutPosition(view) % 2 == 0) {
            outRect.right = mSpace / 2;
        } else {
            outRect.left = mSpace / 2;
        }

        if (parent.getChildLayoutPosition(view) >= mNCollection - 2) {
            outRect.bottom = mSpace;
        }
    }
}
