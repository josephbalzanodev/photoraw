package it.josephbalzano.photoraw.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import it.josephbalzano.photoraw.R;
import it.josephbalzano.photoraw.manager.GlideApp;
import it.josephbalzano.photoraw.model.Photo;

/**
 * Created by Joseph Balzano on 18/08/2017.
 */

public class CuratedPeopleListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "CuratedPeopleListAdapter";

    private static final String PEOPLE_LABEL = "label";
    private static final int PEOPLE_LABEL_VIEW_TYPE = 0;
    private static final int PEOPLE_VIEW_TYPE = 1;

    private List<Photo> mCuratedPhoto = new ArrayList<>();

    public CuratedPeopleListAdapter(List<Photo> curatedPhoto) {
        Photo fakePhoto = new Photo();
        fakePhoto.setId(PEOPLE_LABEL);
        this.mCuratedPhoto.add(0, fakePhoto);
        this.mCuratedPhoto.addAll(curatedPhoto);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == PEOPLE_LABEL_VIEW_TYPE) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.main_popular_people_label, parent, false);
            return new UserLabelHolder(itemView);
        } else if (viewType == PEOPLE_VIEW_TYPE) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.main_popular_people, parent, false);
            return new UserHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserHolder) {
            ((UserHolder) holder).bind(mCuratedPhoto.get(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return !TextUtils.isEmpty(mCuratedPhoto.get(position).getId())
                && mCuratedPhoto.get(position).getId().equals(PEOPLE_LABEL)
                ? PEOPLE_LABEL_VIEW_TYPE : PEOPLE_VIEW_TYPE;
    }

    @Override
    public int getItemCount() {
        return mCuratedPhoto != null ? mCuratedPhoto.size() : 0;
    }

    public class UserHolder extends RecyclerView.ViewHolder {
        private final TextView mAuthorName;
        private final ImageView mAuthorAvatar;
        private final TextView mAuthorNick;

        UserHolder(View itemView) {
            super(itemView);
            mAuthorName = (TextView) itemView.findViewById(R.id.author_name);
            mAuthorNick = (TextView) itemView.findViewById(R.id.author_nickname);
            mAuthorAvatar = (ImageView) itemView.findViewById(R.id.author_avatar);
        }

        public void bind(Photo photo) {
            mAuthorName.setText(photo.getUser().getName());
            mAuthorNick.setText(String.format("@%s", photo.getUser().getUsername()));

            GlideApp.with(itemView.getContext())
                    .load(photo.getUser().getProfileImage().getMedium())
                    .placeholder(R.drawable.avatar_placeholder)
                    .dontAnimate()
                    .into(mAuthorAvatar);
        }
    }

    public class UserLabelHolder extends RecyclerView.ViewHolder {
        public UserLabelHolder(View itemView) {
            super(itemView);
        }
    }
}
