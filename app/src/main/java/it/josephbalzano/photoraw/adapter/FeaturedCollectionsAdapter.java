package it.josephbalzano.photoraw.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import it.josephbalzano.photoraw.R;
import it.josephbalzano.photoraw.manager.GlideApp;
import it.josephbalzano.photoraw.model.Collection;
import it.josephbalzano.photoraw.util.transformation.Blur;
import it.josephbalzano.photoraw.view.activity.SearchResultActivity;
import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static it.josephbalzano.photoraw.view.activity.SearchResultActivity.COLLECTION;

/**
 * Created by Joseph Balzano on 30/08/2017.
 */

public class FeaturedCollectionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Collection> mFeaturedCollections = new ArrayList<>();
    private Activity mParentActivity = null;

    public FeaturedCollectionsAdapter(List<Collection> collections, Activity activity) {
        mFeaturedCollections.addAll(collections);
        mParentActivity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.collection_item, parent, false);

        return new FeaturedCollectionsAdapter.CollectionHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((CollectionHolder) holder).bind(mFeaturedCollections.get(position));
    }

    @Override
    public int getItemCount() {
        return mFeaturedCollections != null ? mFeaturedCollections.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    private class CollectionHolder extends RecyclerView.ViewHolder {
        private final ImageView mPhotoCover;
        private final TextView mTitleCollection;

        public CollectionHolder(View itemView) {
            super(itemView);

            mPhotoCover = (ImageView) itemView.findViewById(R.id.cover_photo);
            mTitleCollection = (TextView) itemView.findViewById(R.id.collection_title);
        }

        public void bind(final Collection collection) {
            RequestOptions options = new RequestOptions();
            options.centerCrop().transform(new BlurTransformation(itemView.getContext()));

            GlideApp.with(mParentActivity.getApplicationContext())
                    .load(collection.getCoverPhoto().getUrls().getRegular())
                    .transform(new Blur(itemView.getContext(), 10))
                    .transition(withCrossFade())
                    .into(mPhotoCover);

            mTitleCollection.setText(collection.getTitle());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent goResult = new Intent(itemView.getContext(), SearchResultActivity.class);
                    goResult.putExtra(COLLECTION, collection);
                    mParentActivity.startActivity(goResult);
                }
            });
        }
    }
}
